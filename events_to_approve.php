<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Eventi da Approvare";

if(!LOGGED_IN || $myrow["role"] != "admin"){
	location(PATH);
}

include(__DIR__."/inc/header.php");
?>
<section id="eventsection">
	<h1>Eventi da approvare</h1>
	<?php
		$events = query("SELECT e.name,
								e.date,
								e.time,
								u.email,
								e.id
						 FROM events e, users u
						 WHERE u.id = e.organizer_user_id AND
							   e.approved = '0'");
		while($event = fetch($events)){
		?>
			<a href="<?=PATH?>event_manage.php?id=<?=$event["id"]?>">
				<div class="infobox">
					<span class="important"><?=entities($event["name"])?></span>
					<br/>
					<footer>
						<p><?=pretty_date(entities($event["date"]))?> <?=entities($event["time"])?><br/>
						<?=entities($event["email"])?> </p>
					</footer>
				</div>
			</a>
		<?php
		}
	?>
</section>

<?php
    include(__DIR__."/inc/footer.php");
?>
