<?php
require_once(__DIR__."/inc/core.php");

if(LOGGED_IN && $myrow["role"] != "customer"){
    location(PATH);
}
$pagetitle = "Carrello";

if(checkpost("do")){
	switch($_POST["do"]){
		case "remove":
			if(checkpost("id")){
				unset($_SESSION["cart"][$_POST["id"]]);
			}
			$output["result"] = "success";
			break;
		case "pay":
			if(!checkpost("sendingmethod") || !in_array($_POST["sendingmethod"], array("physic", "digital"))){
				$output["message"] = "Seleziona un tipo di spedizione valido";
			}elseif(!checkpost("name")){
				$output["message"] = "Inserisci il nome";
			}elseif(!checkpost("surname")){
				$output["message"] = "Inserisci il nome";
			}elseif(!checkpost("birthdate")){
				$output["message"] = "Inserisci la data di nascita";
			}elseif(!checkpost("address")){
				$output["message"] = "Inserisci l'indirizzo";
			}elseif(!checkpost("city")){
				$output["message"] = "Inserisci la citt&agrave;";
			}elseif(!checkpost("zipcode")){
				$output["message"] = "Inserisci il CAP";
			}elseif(!checkpost("country")){
				$output["message"] = "Inserisci la provincia";
			}else{
				$discount = 0;
				if(checkpost("discount")){
					$discount = query("SELECT percentage FROM discounts WHERE code = '".escape($_POST["discount"])."' AND used_times < max_use_times");
					if(num_rows($discount) == 0){
						$discount = -1;
						$_POST["discount"] = "";
					}else{
						$discount = fetch($discount);
						$discount = 1 - ($discount["percentage"] / 100);
					}
				}
				if($discount == -1){
					$output["message"] = "Il codice sconto inserito non &egrave; valido o &egrave; gi&agrave; stato utilizzato troppe volte";
				}else{
					$totalprice = 0;
					while($event = fetch($cartevents)){
						$totalprice += $_SESSION["cart"][$event["id"]]*$event["price"];
					}
					data_seek($cartevents);
					$output["price"] = $totalprice;
					if($totalprice == 0){
						while($event = fetch($cartevents)){
							query("INSERT INTO event_customers (event_id, customer_user_id, sending_method, name, surname, birthdate, address, city, zipcode, country, price, seats) VALUES ('".escape($event["id"])."', '".escape($myrow["id"])."', '".escape($_POST["sendingmethod"])."', '".escape($_POST["name"])."', '".escape($_POST["surname"])."', '".escape($_POST["birthdate"])."', '".escape($_POST["address"])."', '".escape($_POST["city"])."', '".escape($_POST["zipcode"])."', '".escape($_POST["country"])."', '".escape($event["price"]*$discount)."', '".escape($_SESSION["cart"][$event["id"]])."')");

							$ticketscount = 0;
							$tickets = query("SELECT seats FROM event_customers WHERE event_id = '".escape($event["id"])."'");
							while($ticket = fetch($tickets)){
								$ticketscount += $ticket["seats"];
							}
							$output["minchia"] = $ticketscount." == ".$event["seats"];
							if($ticketscount == $event["seats"]){
								// sold out
								$organizer = fetch(query("SELECT name, email FROM users WHERE id = '".escape($event["organizer_user_id"])."'"));
								send_email($organizer["email"], entities($event["name"])." &egrave; SOLD OUT!", entities($organizer["name"]).", il tuo evento <b>".entities($event["name"])."</b> &egrave; ora <b>SOLD OUT</b>! Significa che hai venduto TUUUUUTTI i biglietti che potevi, grazie ad Outside!<br />Ora non ti resta che preparare l'evento.<br /><br />Per qualsiasi dubbio restiamo a disposizione!");
								query("INSERT INTO user_notifications (user_id, text, datetime, `read`) VALUES ('".escape($event["organizer_user_id"])."', '".escape("Il tuo evento <b>".entities($event["name"])."</b> &egrave; SOLD OUT. <a href=\"".PATH."event.php?id=".$event["id"]."\">Clicca qui per visualizzarlo</a>.")."', NOW(), '0')");
							}
						}
						unset($_SESSION["cart"]);
						unset($_SESSION["cartdetails"]);
					}else{
						$_SESSION["cartdetails"] = array_merge($_POST, array("discount" => $_POST["discount"]));
					}
					$output["result"] = "success";
				}
			}
			break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<a class="fullbuttoncontainer" href="<?=PATH?>tickets.php">
	<button aria-label="biglietti già acquistati">Visualizza i biglietti gi&agrave; acquistati</button>
</a>
<section id="cartsection">
	<h1>Carrello</h1>
	<?php
		$totalprice = -1;
		if(!isset($_SESSION["cart"]) || count($_SESSION["cart"]) == 0){
			echo "<div class='main italic'>Non hai ancora preso alcun biglietto</div>";
		}else{
			$totalprice = 0;
			$eventslist = array();
			$events = query("SELECT e.id, e.name, DATE_FORMAT(e.date, '%d-%m-%Y') as date, e.price, a.name as artist_name FROM events e, artists a, event_artists ea WHERE e.approved = '1' AND e.id = ea.event_id AND a.id = ea.artist_id AND e.id IN ('".join("','", array_keys($_SESSION["cart"]))."')");
			while($event = fetch($events)){
				$eventslist[$event["id"]] = $event;
			}
			foreach($_SESSION["cart"] as $eventid => $tickets){
				$event = $eventslist[$eventid];
				$totalprice += $tickets*$event["price"];
			?>
				<a href="<?=PATH?>event.php?id=<?=$event["id"]?>">
					<div class="infobox" data-eventid="<?=$event["id"]?>">
						<button class="custom remove">✕</button>
						<span class="important"><?=entities($event["name"])?></span>
						<br/>
						<?=entities($event["date"])?> - di <?=entities($event["artist_name"])?>
						<footer>
							<?=entities($tickets)?> biglietti - <?=($event["price"]==0 ? "GRATIS" : number_format($tickets*$event["price"], 2, ",", "")."&euro;")?>
						</footer>
					</div>
				</a>
			<?php
			}
		?>

		<?php
		}
	?>
</section>
<script>
	$("section#cartsection div.infobox button.remove").on("click", function(e){
		let eventid = $(this).parents("div.infobox").attr("data-eventid");
		e.preventDefault();
		openAlert({
			title: "Sei sicuro?",
			text: "Vuoi rimuovere questo elemento dal carrello?",
			okbutton: {
				text: "S&igrave;, elimina",
				onclick: function(){
					openAlert({ text: "<div class=\"loading\"></div>" });
					post({
						data: {
							"do": "remove",
							"id": eventid
						},
						callback: function(data){
							if(checkData(data)){
								reload();
							}
						}
					});
				},
				close: false
			},
			cancelbutton: {
				text: "No, annulla"
			}
		});
	});
</script>
<?php if($totalprice > -1){ ?>
	<?php if(!LOGGED_IN){ ?>
		<section class="margintop">
			<h1>Accedi per continuare</h1>
			<p>

				Per poter procedere con l'acquisto dei biglietti devi aver effettuato l'accesso. Se non hai un account, questo &egrave; il momento giusto per crearlo!
				<a href="<?=PATH?>login.php">
					<button>Accedi, se hai gi&agrave; un account</button>
				</a>
				<a href="<?=PATH?>customer_registration.php">
					<button class="empty">Registrati, se non ce l'hai</button>
				</a>
			</p>
		</section>
	<?php }else{ ?>
			<section id="paymentsection" class="margintop">
				<h1>Verifica i dati di fatturazione</h1>
				<p>
					Puoi scegliere se farti spedire i biglietti a casa oppure via email. Nel caso in cui tu voglia averli a casa, assicurati che l'indirizzo di spedizione qui sotto sia corretto o, eventualmente, cambialo. Nota che l'invio di biglietti via posta pu&ograve; richiedere fino a 5 giorni lavorativi. Non forniamo rimborsi in caso di smarrimento o ritardo nella consegna.
				</p>
				<form>
					<select id="customerdatasection_sendingmethod" name="sendingmethod">
						<option value="physic"<?=(isset($_SESSION["cartdetails"]["sendingmethod"]) && $_SESSION["cartdetails"]["sendingmethod"]=="physics"?" selected":"")?>>Spedizione in formato fisico, a casa</option>
						<option value="digital"<?=(isset($_SESSION["cartdetails"]["sendingmethod"]) && $_SESSION["cartdetails"]["sendingmethod"]=="digital"?" selected":"")?>>Spedizione in formato digitale, via email</option>
					</select>
					<label for="customerdatasection_sendingmethod">Tipo di spedizione</label>
					<input type="text" id="customerdata_name" name="name" value="<?=(isset($_SESSION["cartdetails"]["name"])?$_SESSION["cartdetails"]["name"]:$myrow["name"])?>" />
					<label for="customerdataa_name">Nome</label>
					<input type="text" id="customerdata_surname" name="surname" value="<?=(isset($_SESSION["cartdetails"]["surname"])?$_SESSION["cartdetails"]["surname"]:$myrow["surname"])?>" />
					<label for="customerdata_surname">Cognome</label>
					<input type="email" id="customerdata_registrationemail" name="email" value="<?=$myrow["email"]?>" disabled />
					<label for="customerdata_registrationemail">Email</label>
					<input type="date" id="customerdata_birthdate" name="birthdate" value="<?=(isset($_SESSION["cartdetails"]["birthdate"])?$_SESSION["cartdetails"]["birthdate"]:$myrow["birthdate"])?>" />
					<label for="customerdata_birthdate">Data di nascita</label>
					<input type="text" id="customerdata_address" name="address" value="<?=(isset($_SESSION["cartdetails"]["address"])?$_SESSION["cartdetails"]["address"]:$myrow["address"])?>" />
					<label for="customerdata_address">Indirizzo di residenza</label>
					<input type="text" id="customerdata_city" name="city" value="<?=(isset($_SESSION["cartdetails"]["city"])?$_SESSION["cartdetails"]["city"]:$myrow["city"])?>" />
					<label for="customerdata_city">Città</label>
					<input type="text" id="customerdata_zipcode" name="zipcode" value="<?=(isset($_SESSION["cartdetails"]["zipcode"])?$_SESSION["cartdetails"]["zipcode"]:$myrow["zipcode"])?>" />
					<label for="customerdata_zipcode">CAP</label>
					<input type="text" id="customerdata_country" name="country" value="<?=(isset($_SESSION["cartdetails"]["country"])?$_SESSION["cartdetails"]["country"]:$myrow["country"])?>" />
					<label for="customerdata_country">Provincia</label>
					<br /><br />
					<?php if($totalprice > 0){ ?>
						<h2>TOTALE: <?=number_format($totalprice, 2, ",", "")?>&euro;</h2>
						<input type="text" id="customerdata_discount" name="discount" value="<?=(isset($_SESSION["cartdetails"]["discount"])?$_SESSION["cartdetails"]["discount"]:"")?>" />
						<label for="customerdata_country">Codice sconto</label>
						<input type="submit" name="pay" aria-label="procedi al pagamento" value="Procedi al pagamento" />
					<?php }else{ ?>
						<div class="main italic">Poich&eacute; stai acquistando dei biglietti gratuiti, puoi direttamente procedere con la conferma della prenotazione. Ti chiediamo di prenotare soltanto i biglietti davvero necessari.</div>
					    <input type="submit" name="pay" aria-label="conferma la prenotazione" value="Conferma la prenotazione" />
					<?php } ?>
				</form>
			</section>
		</form>
		<script>
			$("section#paymentsection form").on("submit", function(e){
				e.preventDefault();
				formPost("paymentsection", function(data){ console.log(data);
					if(checkData(data)){
						if(data["price"] == 0){
							openAlert({
								title: "Acquisto completato!",
								text: "I biglietti prenotati sono ora disponibili nel tuo riepilogo biglietti. Se hai scelto la modalit&agrave; di spedizione digitale, controlla la tua email, altrimenti aspetta qualche giorno per la spedizione fisica. La consegna pu&ograve; richiedere fino a 5 giorni lavorativi. Ricorda di stampare e portare con te il biglietto!",
								okbutton: {
									text: "Vai al riepilogo biglietti",
									onclick: function(){
										loc("<?=PATH?>tickets.php");
									}
								}
							});
						}else{
							openAlert({
								title: "Attendi",
								text: "Stiamo per aprire il modulo di pagamento. Nel frattempo, non chiudere questa pagina.<div class=\"loading\"></div>",
							});
							setTimeout(function(){
								let w = 700;
								let h = 600;
								let left = (screen.width) ? (screen.width-w)/2 : 0;
								let top = (screen.height) ? (screen.height-h)/2 : 0;
								let win = window.open("<?=PATH?>pay.php?price="+data["price"], "", 'height='+h+',width='+w+',top='+top+',left='+left+',scrollbars=no,resizable');
								if(win.window.focus){
									win.window.focus();
								}
							}, 2000);
						}
					}
				});
			});
		</script>
	<?php } ?>
<?php } ?>
<script>
function confirmPayment(){
	openAlert({
		title: "Acquisto completato!",
		text: "I biglietti acquistati sono ora disponibili nel tuo riepilogo biglietti. Se hai scelto la modalit&agrave; di spedizione digitale, controlla la tua email, altrimenti aspetta qualche giorno per la spedizione fisica. La consegna pu&ograve; richiedere fino a 5 giorni lavorativi. Ricorda di stampare e portare con te il biglietto!",
		okbutton: {
			text: "Vai al riepilogo biglietti",
			onclick: function(){
				loc("<?=PATH?>tickets.php");
			}
		}
	});
}
</script>
<?php
    include(__DIR__."/inc/footer.php");
?>
