<?php
require_once(__DIR__."/inc/core.php");

if(!checkget("code")){
	location(PATH);
}
$user = query("SELECT u.id, u.email FROM users u, customers c WHERE u.id = c.user_id AND CONCAT(u.
	id,'-',c.confirmation_code) = '".addslashes($_GET["code"])."'");
if(num_rows($user) != 1){
	location(PATH);
}
$user = fetch($user);
query("UPDATE customers SET confirmation_code = '' WHERE user_id = '".addslashes($user["id"])."'");

$pagetitle = "Accedi";

include(__DIR__."/inc/header.php");
?>
<section id="registrationadminsection">
	<h1>Registrazione completata!</h1>
	<p>
		Il tuo account è stato verificato con successo. Ora puoi procedere al login.
		<a href="<?=PATH?>login.php?email=<?=entities($user["email"])?>">
			<button>Effettua l'accesso</button>
		</a>
	</p>
</section>
<script>
	$("section#registrationadminsection input[name='adminregistration']").on("click", function(e){
		e.preventDefault();
		formPost("registrationadminsection", function(data){
			if(checkData(data)){
				reload();
			}
		});
	});
</script>
<?php
include(__DIR__."/inc/footer.php");
?>
