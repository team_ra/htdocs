<?php
require_once(__DIR__."/inc/core.php");

if(!LOGGED_IN){
    location(PATH);
}

$pagetitle = "Informazioni Evento";

if(checkget("id")){
    $event = query("SELECT
                    e.id,
					e.name,
					e.image,
					e.program,
					e.address,
					e.seats,
					e.price,
					e.time,
					a.name as artist_name,
                    c.name as category_name,
					e.date,
                    e.approved
				FROM
					events e,
					artists a,
					event_artists ea,
                    categories c
				WHERE
					e.id = ea.event_id AND
					a.id = ea.artist_id AND
                    c.id = e.category_id AND
                    e.id = '".escape($_GET["id"])."'");
    if(num_rows($event) != 1){
        location(PATH);
    }
    $event = fetch($event);
	$event["seats_picked"] = 0;
	$tickets = query("SELECT seats FROM event_customers WHERE event_id = '".escape($event["id"])."'");
	while($ticket = fetch($tickets)){
		$event["seats_picked"] += $ticket["seats"];
	}
}

if(isset($_FILES["eventimage"])){
	$error = false;
	$imageinfo = getimagesize($_FILES["eventimage"]["tmp_name"]);
	if(!in_array($imageinfo["mime"], array("image/jpeg", "image/gif", "image/png"))){
		$error = "Seleziona un'immagine valida (".$imageinfo["mime"].")";
	}else{
		switch($imageinfo["mime"]){
			case "image/jpeg": $extension = "jpg"; break;
			case "image/gif": $extension = "gif"; break;
			case "image/png": $extension = "png"; break;
		}
		$folder = __DIR__ ."/contents/uploads/";
		if(!file_exists($folder)){
			mkdir($folder, 0777);
		}
		$filename = time()."-".rand(1000,9999).".".$extension;
		if($imageinfo["mime"] == "image/jpeg"){
			$exif = exif_read_data($_FILES["eventimage"]["tmp_name"]);
			if(!empty($exif["Orientation"])){
				$imageResource = imagecreatefromjpeg($_FILES["eventimage"]["tmp_name"]);
				switch($exif["Orientation"]){
					case 3:
						$image = imagerotate($imageResource, 180, 0);
					break;
					case 6:
						$image = imagerotate($imageResource, -90, 0);
					break;
					case 8:
						$image = imagerotate($imageResource, 90, 0);
					break;
					default:
						$image = $imageResource;
				}
				imagejpeg($image, $_FILES["eventimage"]["tmp_name"], 90);
				$imageinfo = getimagesize($_FILES["eventimage"]["tmp_name"]);
				imagedestroy($image);
			}
		}
		move_uploaded_file($_FILES["eventimage"]["tmp_name"], $folder.$filename);
	?>
		<script>
			top.setPreview("<?=$filename?>");
		</script>
	<?php
	}
	if($error !== false){
?>
	<script type="text/javascript">
		top.openAlert({
			text: "<?php echo addslashes($error); ?>",
			okbutton: {
				text: "Ok"
			}
		});
	</script>
<?php
	}
	exit;
}

if(checkpost("do")){
    switch($_POST["do"]){
        case "addnewevent":
            if($myrow["role"] == "organizer"){
                if(!checkpost("eventname")){
                    $output["message"] = "Inserisci il nome dell'evento";
                }elseif(!checkpost("type")){
                    $output["message"] = "Inserisci una categoria per l'evento";
                }elseif(!checkpost("image")){
                    $output["message"] = "Carica un'immagine relativa al tuo evento";
                }elseif(!checkpost("eventdate")){
                    $output["message"] = "Inserisci la data dell'evento";
                }elseif(!checkpost("eventtime")){
                    $output["message"] = "Inserisci l'orario dell'evento";
                }elseif(!checkpost("numberofseats")){
                    $output["message"] = "Inserisci il numero di posti disponibili";
                }elseif(is_int($_POST["numberofseats"])){
                    $output["message"] = "Inserisci un valore intero";
                }elseif(!checkpost("eventaddress")){
                    $output["message"] = "Inserisci il luogo dove si terr&agrave l'evento";
                }elseif(!checkpost("artistname")){
                    $output["message"] = "Inserisci il nome dell'artista";
                }elseif(!checkpost("eventdescription")){
                    $output["message"] = "Inserisci il programma dell'evento";
                }elseif(!checkpost("eventprice")){
                    $output["message"] = "Inserisci il prezzo del biglietto per l'evento";
                }elseif(!is_numeric($_POST["eventprice"])){
                    $output["message"] = "Il prezzo del biglietto inserito non &egrave; valido";
                }else{
                    $eventdate = explode("-", $_POST["eventdate"]);
                    if(count($eventdate) != 3 || !checkdate($eventdate[1], $eventdate[2], $eventdate[0]) || strtotime($_POST["eventdate"]) <= time()){
                        $output["message"] = "La data dell'evento inserita non &egrave; valida";
                    }else{
						if(fetch(query("SELECT null FROM artists WHERE name = '".escape($_POST["artistname"])."'")) != 1){
	                        query("INSERT INTO artists (name) VALUES ('".escape($_POST['artistname'])."')");
	                    }
                        $artist_id = fetch(query("SELECT id FROM artists WHERE name = '".escape($_POST["artistname"])."'"))["id"];
                        query("INSERT INTO events (name, category_id, image, organizer_user_id, date, time, seats, address, program, price)
                               VALUES ('".escape($_POST['eventname'])."',
							   		'".escape($_POST["type"])."',
									'".escape($_POST["image"])."',
									'".escape($myrow["id"])."',
									'".escape($_POST["eventdate"])."',
									'".escape($_POST["eventtime"])."',
									'".escape($_POST["numberofseats"])."',
									'".escape($_POST["eventaddress"])."',
									'".escape($_POST["eventdescription"])."',
									'".escape($_POST["eventprice"])."'
								)");
                        $event_id = insert_id();
                        query("INSERT INTO event_artists (artist_id, event_id) VALUES ('".escape($artist_id)."', '".escape($event_id)."')");
						$admins = query("SELECT * FROM admins");
						while($admin = fetch($admins)){
							query("INSERT INTO user_notifications (user_id, text, datetime, `read`) VALUES ('".escape($admin["user_id"])."', '".escape("C'&egrave; un nuovo evento in attesa di approvazione. <a href=\"".PATH."event_manage.php?id=".$event_id."\">Clicca qui per visualizzarlo</a>.")."', NOW(), '0')");
						}
                        $output["result"] = "success";
                    }
                }
            }
        break;
        case "managevent":
            if($myrow["role"] == "admin" || $myrow["role"] == "organizer"){
                if(!checkpost("eventdate")){
                    $output["message"] = "Inserisci la data dell'evento";
                }elseif(!checkpost("eventtime")){
                    $output["message"] = "Inserisci l'orario dell'evento";
                }elseif(!checkpost("numberofseats")){
                    $output["message"] = "Inserisci il numero di posti disponibili";
                }elseif(is_int($_POST["numberofseats"])){
                    $output["message"] = "Inserisci un valore intero";
                }elseif(!checkpost("eventaddress")){
                    $output["message"] = "Inserisci il luogo dove si terr&agrave l'evento";
                }elseif(!checkpost("eventdescription")){
                    $output["message"] = "Inserisci il programma dell'evento";
                }else{
                    $eventdate = explode("-", $_POST["eventdate"]);
                    if(count($eventdate) != 3 || !checkdate($eventdate[1], $eventdate[2], $eventdate[0]) || strtotime($_POST["eventdate"]) <= time()){
                        $output["message"] = "La data dell'evento inserita non &egrave; valida";
                    }else{
                        $id_artist=fetch(query("SELECT id FROM artists WHERE name = '".escape($_POST["artistname"])."'"))["id"];
                        query("UPDATE events SET date = '".escape($_POST["eventdate"])."',
                                                 time = '".escape($_POST["eventtime"])."',
                                                 seats = '".escape($_POST["numberofseats"])."',
                                                 address = '".escape($_POST["eventaddress"])."',
                                                 program = '".escape($_POST["eventdescription"])."'
                               WHERE id = '".escape($event["id"])."'");
						$customers = query("SELECT DISTINCT customer_user_id FROM event_customers WHERE event_id = '".escape($event["id"])."'");
						$notification_text = "L'evento <b>".entities($event["name"])."</b> &egrave; stato modificato. <a href=\"".PATH."event.php?id=".$event["id"]."\">Clicca qui per visualizzarlo</a>.";
						while($customer = fetch($customers)){
							query("INSERT INTO user_notifications (user_id, text, datetime, `read`) VALUES ('".escape($customer["customer_user_id"])."', '".escape($notification_text)."', NOW(), '0')");
						}
                        $output["result"] = "success";
                    }
                }
            }
        break;
        case "eventapprove":
			if($myrow["role"] == "admin"){
				query("UPDATE events SET approved = '1' WHERE id = '".escape($event["id"])."'");
				$output["result"] = "success";
			}
			break;
    }
    output();
}

include(__DIR__."/inc/header.php");
?>

<?php
    if(!isset($event)){
    ?>
        <section id="addnewevent">
            <h1>Aggiungi Evento</h1>
			<form method="post" enctype="multipart/form-data" target="hidden-iframe">
				<p>
					Clicca qui per selezionare un'immagine di anteprima dell'evento dal tuo computer (PNG, JPG O GIF)
					<input name="eventimage" id="eventimage" type="file" accept="image/*">
					<label for="eventimage">Immagine</label>
				</p>
				<iframe name="hidden-iframe"></iframe>
			</form>
			<div id="eventpreview_container" class="imagecontainer">
				<img id="eventpreview" style="display: none;">
			</div>
            <form>
				<input type="hidden" name="image" />
                <input type="text" id="eventname" name="eventname"/>
                <label for="eventname">Nome Evento</label>
                <select name="type" id="eventcategories_type">
                    <?php
						$categories = query("SELECT * FROM categories");
						while($category = fetch($categories)){
							echo "<option value=\"".$category["id"]."\">".entities(ucfirst($category["name"]))."</option>";
						}
					?>
                </select>
                <label for="eventcategories_type">Categoria</label>
                <input type="date" id="eventdate" name="eventdate"/>
                <label for="eventdate">Data</label>
                <input type="time" id="eventtime" name="eventtime"/>
                <label for="eventtime">Ora</label>
                <input type="number" id="numberofseats" name="numberofseats"/>
                <label for="numberofseats">Numero Posti</label>
                <input type="text" id="artistname" name="artistname"/>
                <label for="artistname">Nome Artista</label>
                <input type="text" id="eventaddress" name="eventaddress"/>
                <label for="eventaddress">Luogo Evento</label>
                <input type="text" id="eventdescription" name="eventdescription"/>
                <label for="eventdescription">Dettagli dell'evento</label>
                <input type="text" id="eventprice" name="eventprice"/>
                <label for="eventprice">Prezzo Biglietto</label>
                <input type="submit" name="addnewevent" value="Genera il nuovo evento" />
            </form>
        </section>
        <script type="text/javascript">
			function setPreview(url){
				$("img#eventpreview").attr("src", "<?=PATH?>contents/uploads/"+url).show();
				$("section#addnewevent input[name='image']").val(url);
				closeAlert();
			}
			$("input[name='eventimage']").on("change", function(){
				openAlert({ text: "<div class=\"loading\"></div>" });
				$(this).parents("form").submit();
			});
			$("section#addnewevent form:not([method])").on("submit", function(e){
				e.preventDefault();
				formPost("addnewevent", function(data){
					if(checkData(data)){
						openAlert({
							title: "Fatto",
							text: "L'evento è stato creato con successo ed è stato inoltrato all'amministratore; attendi l'approvazione",
							okbutton: {
								text: "Ok",
								onclick: function(){
									loc("<?=PATH?>events_list.php");
								},
								close: false
							}
						});
					}
				});
			});
		</script>
    <?php
    }elseif($myrow["role"]=="organizer" || ($myrow["role"]=="admin" && $event["approved"]=="1")){
    ?>
        <section id="managevent">
            <h2>Informazioni sull'Evento</h2>
            <form>
                <input type="text" id="eventname" name="eventname" value="<?=entities($event["name"])?>" disabled />
                <label for="eventname">Nome Evento</label>
                <input type="text" id="eventcategory" name="eventcategory"value="<?=entities($event["category_name"])?>" disabled/>
                <label for="eventcategory">Categoria</label>
                <input type="text" id="artistname" name="artistname" value="<?=entities($event["artist_name"])?>" disabled/>
                <label for="artistname">Nome Artista</label>
                <input type="text" id="eventprice" name="eventprice"value="<?=entities($event["price"])?>" disabled/>
                <label for="eventprice">Prezzo Biglietto</label>
                <input type="date" id="eventdate" name="eventdate" value="<?=entities($event["date"])?>"/>
                <label for="eventdate">Data</label>
                <input type="time" id="eventtime" name="eventtime" value="<?=entities($event["time"])?>"/>
                <label for="eventtime">Ora</label>
                <input type="number" id="numberofseats" name="numberofseats" value="<?=entities($event["seats"])?>"/>
                <label for="numberofseats">Numero Posti</label>
                <input type="text" id="eventaddress" name="eventaddress" value="<?=entities($event["address"])?>"/>
                <label for="eventaddress">Luogo Evento</label>
                <input type="text" id="eventdescription" name="eventdescription" value="<?=entities($event["program"])?>"/>
                <label for="eventdescription">Dettagli dell'evento</label>
                <input type="submit" name="managevent" value="Aggiorna" />
            </form>
        </section>
        <script type="text/javascript">
			$("section#managevent form").on("submit", function(e){
				e.preventDefault();
				formPost("managevent", function(data){
					if(checkData(data)){
						openAlert({
							title: "Fatto",
							text: "L'evento è stato modificato con successo",
							okbutton: {
								text: "Ok",
								onclick: function(){
									loc("<?=PATH?>event.php?id=<?=$_GET["id"]?>");
								},
								close: false
							}
						});
					}
				});
			});
        </script>
    <?php
    }elseif($myrow["role"]=="admin" && $event["approved"]=="0"){
    ?>
        <section id="eventapprove">
            <h2>Informazioni sull'Evento</h2>
            <form>
                <input type="text" id="eventname" name="eventname" value="<?=entities($event["name"])?>" disabled />
                <label for="eventname">Nome Evento</label>
                <input type="text" id="eventcategory" name="eventcategory"value="<?=entities($event["category_name"])?>" disabled />
                <label for="eventcategory">Categoria</label>
                <input type="text" id="artistname" name="artistname" value="<?=entities($event["artist_name"])?>" disabled />
                <label for="artistname">Nome Artista</label>
                <input type="text" id="eventprice" name="eventprice"value="<?=entities($event["price"])?>" disabled />
                <label for="eventprice">Prezzo Biglietto</label>
                <input type="date" id="eventdate" name="eventdate" value="<?=entities($event["date"])?>" disabled />
                <label for="eventdate">Data</label>
                <input type="time" id="eventtime" name="eventtime" value="<?=entities($event["time"])?>" disabled />
                <label for="eventtime">Ora</label>
                <input type="number" id="numberofseats" name="numberofseats" value="<?=entities($event["seats"])?>" disabled />
                <label for="numberofseats">Numero Posti</label>
                <input type="text" id="eventaddress" name="eventaddress" value="<?=entities($event["address"])?>" disabled />
                <label for="eventaddress">Luogo Evento</label>
                <input type="text" id="eventdescription" name="eventdescription" value="<?=entities($event["program"])?>" disabled />
                <label for="eventdescription">Dettagli dell'evento</label>
                <input type="submit" name="eventapprove" value="Approva l'evento" />
            </form>
        </section>
    <?php
    }
    ?>

<?php
include(__DIR__."/inc/footer.php");
?>
