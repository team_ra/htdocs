<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Account";

if(!LOGGED_IN){
    location(PATH."login.php");
}

if(checkpost("do")){
	switch($_POST["do"]){
		case "changepassword":
			if(!checkpost("oldpassword")){
				$output["message"] = "Inserisci la password attuale";
			}elseif(!checkpost("newpassword")){
				$output["message"] = "Inserisci la nuova password";
			}elseif(strlen($_POST["newpassword"]) < 6){
				$output["message"] = "La nuova password deve essere lunga almeno 6 caratteri";
			}elseif(!checkpost("repeatnewpassword") || $_POST["newpassword"] != $_POST["repeatnewpassword"]){
				$output["message"] = "Le due password inserite non coincidono";
			}elseif(password($myrow["email"], $_POST["oldpassword"]) != $myrow["password"]){
                $output["message"] = "La password attuale non è corretta";
            }else{
                query("UPDATE users SET password = '".escape(password($myrow["email"], $_POST["newpassword"]))."' WHERE id = '".escape($myrow["id"])."'");
				$_SESSION["password"] = $_POST["newpassword"];
                $output["result"] = "success";
            }
			break;
		case "editcustomerdata":
			if($myrow["role"] == "customer"){
				if(checkpost("address")){
					query("UPDATE customers SET address = '".escape($_POST["address"])."' WHERE user_id = '".escape($myrow["id"])."'");
					$output["result"] = "success";
				}
				if(checkpost("city")){
					query("UPDATE customers SET city = '".escape($_POST["city"])."' WHERE user_id = '".escape($myrow["id"])."'");
					$output["result"] = "success";
				}
				if(checkpost("zipcode")){
					if(!is_numeric($_POST["zipcode"]) || strlen($_POST["zipcode"]) != 5){
						$output["message"] = "Il codice postale (CAP) inserito non &egrave; valido";
					} else{
						query("UPDATE customers SET zipcode = '".escape($_POST["zipcode"])."' WHERE user_id = '".escape($myrow["id"])."'");
						$output["result"] = "success";
					}
				}
				if(checkpost("country")){
					if(strlen($_POST["country"]) != 2){
						$output["message"] = "Inserisci la provincia di residenza su 2 lettere";
					}else{
						query("UPDATE customers SET country = '".escape($_POST["country"])."' WHERE user_id = '".escape($myrow["id"])."'");
						$output["result"] = "success";
					}
				}
			}
			break;
		case "editorganizerdata":
			if($myrow["role"] == "organizer"){
				if(checkpost("website")){
					query("UPDATE organizers SET website = '".escape($_POST["website"])."' WHERE user_id = '".escape($myrow["id"])."'");
					$output["result"] = "success";
				}
				if(checkpost("phone_number")){
					query("UPDATE organizers SET phone_number = '".escape($_POST["phone_number"])."' WHERE user_id = '".escape($myrow["id"])."'");
					$output["result"] = "success";
				}
			}
			break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<a class="fullbuttoncontainer" href="<?=PATH?>logout.php">
	<button aria-label="disconnettiti">Disconnettiti</button>
</a>
<section id="notifications" class="margintop">
	<?php
		$notifications_unread = query("SELECT *, DATE_FORMAT(datetime, '%d-%m-%Y %H:%i') as date_formatted FROM user_notifications WHERE user_id = '".escape($myrow["id"])."' AND `read` = '0'");
		$notifications_read = query("SELECT *, DATE_FORMAT(datetime, '%d-%m-%Y %H:%i') as date_formatted FROM user_notifications WHERE user_id = '".escape($myrow["id"])."' AND `read` = '1'");
	?>
	<h1>Notifiche</h1>
	<h2>Nuove (<?=num_rows($notifications_unread)?>)</h2>
	<?php
		if(num_rows($notifications_unread) == 0){
			echo "<div class=\"main italic\">Non hai nuove notifiche da leggere</div>";
		}else{
			query("UPDATE user_notifications SET `read` = '1' WHERE user_id = '".escape($myrow["id"])."'");
			while($notification = fetch($notifications_unread)){
				echo "<div class=\"infobox\">";
					echo $notification["text"];
					echo "<footer>".$notification["date_formatted"]."</footer>";
				echo "</div>";
			}
		}
	?>
	<?php if(num_rows($notifications_read) > 0){ ?>
		<button class="empty" data-action="showhideread">Mostra le notifiche gi&agrave; lette</button>
		<div data-section="read" data-visible="false" style="display: none;">
			<br />
			<h2>Gi&agrave; lette (<?=num_rows($notifications_read)?>)</h2>
			<?php
				while($notification = fetch($notifications_read)){
					echo "<div class=\"infobox\">";
						echo $notification["text"];
						echo "<footer>".$notification["date_formatted"]."</footer>";
					echo "</div>";
				}
			?>
		</div>
	<?php } ?>
</section>
<script>
	$("section#notifications button[data-action='showhideread']").on("click", function(){
		let readsection = $("section#notifications div[data-section='read']");
		if(readsection.attr("data-visible") == "true"){
			readsection.attr("data-visible", "false");
			readsection.slideUp();
			$(this).html("Mostra le notifiche gi&agrave; lette");
		}else{
			readsection.attr("data-visible", "true");
			readsection.slideDown();
			$(this).html("Nascondi le notifiche gi&agrave; lette");
		}
	});
</script>
<section id="changepassword" class="margintop">
	<h1>Cambia Password</h1>
	<form>
		<input type="password" id="changepassword_oldpassword" name="oldpassword" />
		<label for="changepassword_oldpassword">Password attuale</label>
		<input type="password" id="changepassword_newpassword" name="newpassword" />
		<label for="changepassword_newpassword">Nuova password</label>
		<input type="password" id="changepassword_repeatnewpassword" name="repeatnewpassword" />
		<label for="changepassword_repeatnewpassword">Ripeti la nuova password</label>
		<input type="submit" name="changepassword" value="Cambia password" />
	</form>
</section>
<script type="text/javascript">
	$("section#changepassword form").on("submit", function(e){
		e.preventDefault();
		formPost("changepassword", function(data){
			if(checkData(data)){
				openAlert({
					title: "Fatto",
					text: "La tua password &egrave; stata aggiornata con successo",
					okbutton: {
						text: "Ok",
						onclick: function(){
							reload();
						},
						close: false
					}
				});
			}
		});
	});
</script>
<?php
switch($myrow["role"]){
	case "admin":
?>
		<section id="editadmindata" class="margintop">
			<h1>I tuoi dati</h1>
			<form>
				<input type="text" id="editadmindata_name" name="name" value="<?=$myrow["name"]?>" disabled />
				<label for="editadmindata_name">Nome</label>
				<input type="text" id="editadmindata_surname" name="surname" value="<?=$myrow["surname"]?>" disabled />
				<label for="editadmindata_surname">Cognome</label>
				<input type="email" id="editadmindata_registrationemail" name="email" value="<?=$myrow["email"]?>" disabled />
				<label for="editadmindata_registrationemail">Email</label>
				</form>
		</section>
<?php
		break;
	case "customer":
?>
		<section id="editcustomerdata" class="margintop">
			<h1>I tuoi dati</h1>
			<form>
				<input type="text" id="editcustomerdata_name" name="name" value="<?=$myrow["name"]?>" disabled />
				<label for="editcustomerdata_name">Nome</label>
				<input type="text" id="editcustomerdata_surname" name="surname" value="<?=$myrow["surname"]?>" disabled />
				<label for="editcustomerdata_surname">Cognome</label>
				<input type="email" id="editcustomerdata_registrationemail" name="email" value="<?=$myrow["email"]?>" disabled />
				<label for="editcustomerdata_registrationemail">Email</label>
				<input type="date" id="editcustomerdata_birthdate" name="birthdate" value="<?=$myrow["birthdate"]?>" disabled />
				<label for="editcustomerdata_birthdate">Data di nascita</label>
				<input type="text" id="editcustomerdata_address" name="address" value="<?=$myrow["address"]?>" />
				<label for="editcustomerdata_address">Indirizzo di Residenza</label>
				<input type="text" id="editcustomerdata_city" name="city" value="<?=$myrow["city"]?>" />
				<label for="editcustomerdata_city">Citt&agrave</label>
				<input type="text" id="editcustomerdata_zipcode" name="zipcode" value="<?=$myrow["zipcode"]?>" />
				<label for="editcustomerdata_zipcode">CAP</label>
				<input type="text" id="editcustomerdata_country" name="country" value="<?=$myrow["country"]?>" />
				<label for="editcustomerdata_country">Provincia</label>
				<input type="submit" name="editcustomerdata" value="Aggiorna" />
			 </form>
		</section>
		<script type="text/javascript">
			$("section#editcustomerdata form").on("submit", function(e){
				e.preventDefault();
				formPost("editcustomerdata", function(data){
					if(checkData(data)){
						openAlert({
							title: "Fatto",
							text: "I tuoi dati sono stati aggiornati con successo",
							okbutton: {
								text: "Ok",
								onclick: function(){
									reload();
								},
								close: false
							}
						});
					}
				});
			});
		</script>
<?php
		break;
	case "organizer":
?>
		<section id="editorganizerdata" class="margintop">
			<h1>I tuoi dati</h1>
			<form>
                        <input type="text" id="organizerdata_name" name="name" value="<?=$myrow["name"]?>" disabled />
					    <label for="organizerdata_name">Nome</label>
                        <input type="text" id="organizerdata_surname" name="surname" value="<?=$myrow["surname"]?>" disabled />
                        <label for="organizerdata_surname">Cognome</label>
                        <input type="email" id="organizerdata_registrationemail" name="email" value="<?=$myrow["email"]?>" disabled />
                        <label for="organizerdata_registrationemail">Email</label>
                        <?php
                            if($myrow["type"]=="company"){
                            ?>
                                <input type="text" id="organizerdata_type" name="type" value="Azienda" disabled />
                                <label for="organizerdata_type">Tipo Azienda</label>
                                <input type="text" id="organizerdata_businessname" name="businessname" value="<?=$myrow["business_name"]?>" disabled/>
                                <label for="organizerdata_businessname">Nome Azienda</label>
                                <input type="text" id="organizerdata_idnumber" name="idnumber" value="<?=$myrow["id_number"]?>" disabled/>
                                <label for="organizerdata_idnumber">Partita Iva</label>
                        <?php
                            }else{
                            ?>
                                <input type="text" id="organizerdata_type" name="type" value="Privato" disabled />
                                <label for="organizerdata_type">Tipo di Azienda</label>
                                <input type="text" id="organizerdata_idnumber" name="idnumber" value="<?=$myrow["id_number"]?>" disabled/>
                                <label for="organizerdata_idnumber">Codice Fiscale</label>
                            <?php
                            }
                        ?>
                        <input type="tel" id="organizerdata_phonenumber" name="phone_number" value="<?=$myrow["phone_number"]?>" />
                        <label for="organizerdata_phonenumber">Numero di Telefono</label>
                        <input type="url" id="organizerdata_website" name="website" value="<?=$myrow["website"]?>" />
						<label for="organizerdata_website">Sito Internet</label>
						<input type="submit" name="editorganizerdata" value="Aggiorna" />
                    </form>
		</section>
		<script type="text/javascript">
			$("section#editorganizerdata form").on("submit", function(e){
				e.preventDefault();
				formPost("editorganizerdata", function(data){
					if(checkData(data)){
						openAlert({
							title: "Fatto",
							text: "I tuoi dati sono stati aggiornati con successo",
							okbutton: {
								text: "Ok",
								onclick: function(){
									reload();
								},
								close: false
							}
						});
					}
				});
			});
		</script>
<?php
		break;
}
?>
<?php
    include(__DIR__."/inc/footer.php");
?>
