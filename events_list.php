<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "I tuoi Eventi";

if(!LOGGED_IN || $myrow["role"] != "organizer"){
	location(PATH."events.php");
}

if(checkpost("do")){
    switch($_POST["do"]){
		case "search":
			$wheres = array(
				"ea.artist_id = a.id",
				"ea.event_id = e.id",
				"u.id = '".escape($myrow["id"])."'",
				"e.organizer_user_id = u.id"
			);
			if(checkpost("searchvalue")){
				$wheres[] = "(e.name LIKE '%".escape($_POST["searchvalue"])."%' OR a.name LIKE '%".escape($_POST["searchvalue"])."%')";
			}
			if(checkpost("filterevents") && $_POST["filterevents"] == "notapproved"){
				$wheres[] = "e.approved = '0'";
			}
			if(checkpost("filterevents") && $_POST["filterevents"] == "approved"){
				$wheres[] = "e.approved = '1'";
			}
			$events = query("SELECT
								e.id,
								e.name,
								e.image,
								e.program,
								e.address,
								e.seats,
								a.name as artist_name,
								DATE_FORMAT(e.date, '%d-%m-%Y') as date
							FROM
								events e,
								artists a,
								event_artists ea,
								users u
							WHERE
								".join(" AND ", $wheres)."
							ORDER BY
								e.name");
			$output["events"] = array();
			while($event = fetch($events)){
				$event["seats_picked"] = 0;
				$tickets = query("SELECT seats FROM event_customers WHERE event_id = '".escape($event["id"])."'");
				while($ticket = fetch($tickets)){
					$event["seats_picked"] += $ticket["seats"];
				}
				if(strlen($event["program"]) > 50){
					$event["program"] = trim(substr($event["program"], 0, 50))."...";
				}
				$output["events"][] = $event;
			}
			$output["result"] = "success";
			break;
    }
    output();
}

include(__DIR__."/inc/header.php");
?>
<a class="fullbuttoncontainer" href="<?=PATH?>event_manage.php">
	<button aria-label="crea evento">Crea un nuovo evento</button>
</a>
<section id="eventssection">
	<h1>I tuoi Eventi</h1>
    <form>
	<fieldset>
		<legend>Filtri</legend>
		<div class="filter">
			<label for="eventssection_filter_notapproved">Non approvati</label>
			<input type="radio" name="filterevents" id="eventssection_filter_notapproved" value="notapproved" checked />
		</div>
		<div class="filter">
			<label for="eventssection_filter_approved">Approvati</label>
			<input type="radio" name="filterevents" id="eventssection_filter_approved" value="approved" />
		</div>
	</fieldset>
    	<div class="searchbox">
			<input type="text" name="searchvalue" id="searchvalue" placeholder="Cerca un evento o un artista..." />
			<label class="hidden" for="searchvalue">Cerca</label>
            <input type="submit" name="search" value="" />
		</div>
	</form>
	<div id="eventssection_events"></div>
</section>
<script>
    $("section#eventssection form").on("submit", function(e){
		e.preventDefault();
		$("#eventssection_events").html("<div class=\"loading\"></div>");
        formPost("eventssection", function(data){
			if(checkData(data)){
				let htmloutput = "";
				if(data["events"].length == 0){
					htmloutput += "<div class=\"main italic\">Nessun evento trovato</div>";
				}else{
					htmloutput += "<div class=\"cards\">";
					for(let i=0;i<data["events"].length;i++){
						htmloutput += "<article class=\"card\">";
							htmloutput += "<a href=\"<?=PATH?>event.php?id="+data["events"][i]["id"]+"\">";
								htmloutput += "<img src=\"<?=PATH?>contents/uploads/"+data["events"][i]["image"]+"\" />";
								htmloutput += "<div class=\"info\">";
									// h1: https://html.spec.whatwg.org/multipage/sections.html#the-h1,-h2,-h3,-h4,-h5,-and-h6-elements
									htmloutput += "<header><h1>"+data["events"][i]["name"]+"</h1></header>";
									htmloutput += "<p>"+data["events"][i]["program"]+"</p>";
									htmloutput += "<footer>"+data["events"][i]["date"]+"<br />Posti: "+(data["events"][i]["seats"]-data["events"][i]["seats_picked"])+" su "+data["events"][i]["seats"]+"</footer>";
								htmloutput += "</div>";
							htmloutput += "</a>";
						htmloutput += "</article>";
					}
					htmloutput += "</div>";
				}
				$("#eventssection_events").html(htmloutput);
				$("#eventssection_events img").on("load", function(){
					reloadWaterfall();
				});
				reloadWaterfall();
			}
		}, false);
    });
	$("section#eventssection form").submit();
</script>
<?php
    include(__DIR__."/inc/footer.php");
?>
