<?php
require_once(__DIR__."/inc/core.php");

if(!checkget("id")){
	location(PATH);
}

$pagetitle = "Evento";

$event = query("SELECT
					e.id,
					e.name,
					e.image,
					e.program,
					e.address,
					e.seats,
					e.price,
					TIME_FORMAT(e.time, '%H:%i') as time,
					e.approved,
					e.organizer_user_id,
					c.name as category_name,
					a.name as artist_name,
					DATE_FORMAT(e.date, '%d-%m-%Y') as date
				FROM
					events e,
					artists a,
					event_artists ea,
					categories c
				WHERE
					e.id = ea.event_id AND
					a.id = ea.artist_id AND
					c.id = e.category_id AND
					e.id = '".escape($_GET["id"])."'");
if(num_rows($event) != 1){
	location(PATH);
}
$event = fetch($event);
$event["seats_picked"] = 0;
$tickets = query("SELECT seats FROM event_customers WHERE event_id = '".escape($event["id"])."'");
while($ticket = fetch($tickets)){
	$event["seats_picked"] += $ticket["seats"];
}

if($event["approved"] == '0' && ($myrow["id"] != $event["organizer_user_id"] || $myrow["role"] == "admin")){
	location(PATH);
}

$event["seats_free"] = $event["seats"]-$event["seats_picked"];
$pagetitle = $event["name"];

if(checkpost("do")){
	switch($_POST["do"]){
		case "addtocart":
			$alreadypicked = isset($_SESSION["cart"][$event["id"]]) ? $_SESSION["cart"][$event["id"]] : 0;
			if(!checkpost("amount") || !is_numeric($_POST["amount"]) || $_POST["amount"] <= 0){
				$output["message"] = "Inserisci una quantit&agrave; valida";
			}elseif($event["seats_free"] == 0){
				$output["message"] = "Purtroppo non ci sono pi&ugrave; posti disponibili per questo evento";
			}elseif($_POST["amount"]+$alreadypicked > $event["seats_free"]){
				$output["message"] = "Stai tentando di acquistare ".($_POST["amount"]+$alreadypicked)." biglietti".($alreadypicked>0?" (di cui ".$alreadypicked." precedentemente aggiunti al carrello)":"")." ma, purtroppo, sono rimasti soltanto ".$event["seats_free"]." posti disponibili";
			}else{
				// se ho già dei biglietti di questo evento al carrello devo sommarli, altrimenti li devo aggiungere e basta
				if(!isset($_SESSION["cart"][$event["id"]])){
					// se non esiste, creo nel carrello il riferimento a questo evento
					$_SESSION["cart"][$event["id"]] = 0;
				}
				// aggiungo i biglietti
				$_SESSION["cart"][$event["id"]] += $_POST["amount"];
				$output["result"] = "success";
			}
			break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<a class="fullbuttoncontainer" href="<?=PATH?>events.php">
	<button>Torna all'elenco degli eventi</button>
</a>
<section id="eventsection" class="margintop">
    <img alt="Locandina dell'evento" src="<?=PATH?>contents/uploads/<?=$event["image"]?>" />
    <section>
        <h1><?=entities($event["name"])?></h1>
        <table>
			<tr>
                <th>Categoria</th>
                <td><?=entities(ucfirst($event["category_name"]))?></td>
            </tr>
            <tr>
                <th>Artista</th>
                <td><?=entities($event["artist_name"])?></td>
            </tr>
            <tr>
                <th>Data</th>
                <td><?=pretty_date($event["date"])?></td>
            </tr>
            <tr>
                <th>Ora</th>
                <td><?=$event["time"]?></td>
            </tr>
            <tr>
                <th>Luogo</th>
                <td><?=entities($event["address"])?></td>
            </tr>
            <tr>
                <th>Prezzo Biglietto</th>
                <?php
                if($event["price"]==0){?>
                    <td>Gratuito</td>
                <?php
                }else{?>
                    <td><?=number_format($event["price"], 2, ",", "")?> €</td>
                <?php
                } ?>
            </tr>
            <tr>
                <th>Biglietti Disponibili</th>
				<td>
	                <?php
		                if($event["seats"] == 0){
		                    echo "Illimitati";
		                }elseif($event["seats_free"] == 0){
							echo "Terminati";
		                }else{
							echo $event["seats_free"]." posti disponibili";
						}
	                ?>
				</td>
            </tr>
        </table>
		<br />
        <h2>Programma</h2>
        <p><?=entities($event["program"])?></p>
    </section>
    <?php
        if(LOGGED_IN && ($myrow["role"] == "admin" || $myrow["role"] == "organizer" && $event["organizer_user_id"] == $myrow["id"])){
        ?>
            <a href="<?=PATH?>event_manage.php?id=<?=$event["id"]?>">
                <button class="empty" aria-label="modifica">Modifica le informazioni</button>
            </a>
    	<?php
		}elseif((!LOGGED_IN || $myrow["role"] == "customer")){
			if($event["seats_free"] > 0 && strtotime($event["date"]) >= time()){
    ?>
			<form>
				<input type="hidden" name="amount">
            	<input type="submit" name="addtocart" value="Aggiungi al carrello!" aria-label="carrello">
			</form>
    <?php
			}else{
	?>
				<div class="main italic">L'evento non &egrave; pi&ugrave; disponibile</div>
	<?php
			}
		}
    ?>
</section>
<script>
	$("section#eventsection form").on("submit", function(e){
		e.preventDefault();
		openAlert({
			title: "Aggiungi al carrello",
			text: "Inserisci la quantit&agrave; di biglietti da aggiungere al carrello:<input id=\"alert_amount\" type=\"text\" autofocus /><label for=\"alert_amount\">Quantit&agrave;</label>",
			oncreate: function(){
				$("div#alertcontainer input").val($("section#eventsection input[name='amount']").val());
				$("div#alertcontainer input").on("keyup", function(){
					$("section#eventsection input[name='amount']").val($(this).val());
				});
			},
			okbutton: {
				text: "Aggiungi",
				onclick: function(){
					formPost("eventsection", function(data){
						if(checkData(data)){
							openAlert({
								title: "Fatto",
								text: "Hai aggiunto al carrello questo evento",
								okbutton: {
									text: "Vai al carrello",
									onclick: function(){
										loc("<?=PATH?>cart.php");
									},
									close: false
								},
								cancelbutton: {
									text: "Chiudi"
								}
							});
						}
					});
				},
				close: false
			},
			cancelbutton: {
				text: "Annulla"
			}
		});
	});
</script>

<?php
include(__DIR__."/inc/footer.php");
?>
