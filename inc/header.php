<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8" />
		<title>Outside<?=isset($pagetitle)?" - ".$pagetitle:""?></title>
		<script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>
		<script type="text/javascript" src="<?=PATH?>contents/jquery.js?<?=time()?>"></script>
		<script type="text/javascript" src="<?=PATH?>contents/global.js?<?=time()?>"></script>
		<script type="text/javascript" src="<?=PATH?>contents/content.js?<?=time()?>"></script>
		<script type="text/javascript" src="<?=PATH?>contents/waterfall.js?<?=time()?>"></script>
		<script type="text/javascript" src="<?=PATH?>contents/swiper.js?<?=time()?>"></script>
		<link rel="stylesheet" href="<?=PATH?>contents/styles.css?<?=time()?>"/>
	</head>
	<body>
		<header id="navbar" <?=((!LOGGED_IN || $myrow["role"] == "customer") ? "class=\"cart\"" : "")?>>
			<button class="custom menu" aria-label="open menu">
				<div></div>
			</button>
			<a id="logo" href="<?=PATH?>">
				<img alt="Logo sito: Outside" src="<?=PATH?>contents/logo_white.png"/>
			</a>
			<a href="<?=PATH?>account.php">
				<button class="custom account" aria-label="profilo utente">
					<?php if(LOGGED_IN && $myrow["notifications_count"] > 0){ ?>
						<div class="badge"><?=$myrow["notifications_count"]?></div>
					<?php } ?>
				</button>
				<div class="userinfo">
					<?php
						if(LOGGED_IN){
							echo entities($myrow["name"]);
						}else{
							echo "Accedi o Registrati";
						}
					?>
				</div>
			</a>
			<?php if(!LOGGED_IN || $myrow["role"] == "customer"){ ?>
				<a href="<?=PATH?>cart.php">
					<button class="custom cart" aria-label="carrello">
						<?php if(count($_SESSION["cart"]) > 0){ ?>
							<div class="badge"><?=count($_SESSION["cart"])?></div>
						<?php } ?>
					</button>
				</a>
			<?php } ?>
		</header>
		<div id="menucontainer">
			<nav id="menu">
				<header>
					<button class="custom close" aria-label="chiudi menu">
						<div></div>
					</button>
					MENU
				</header>
				<ul>
					<?php
						$menu = array();
						if(!LOGGED_IN || $myrow["role"] != "admin"){
							$menu[PATH] = "Pagina principale";
						}
						$menu[PATH."events.php"] = "Esplora gli eventi";
						if(LOGGED_IN){
							if($myrow["role"] == "admin" && isset($_SESSION["security_check_ok"])){
								// admin
								$menu[PATH."events_to_approve.php"] = "Gestione eventi";
								$menu[PATH."users_list.php"] = "Gestione utenti";
							}elseif($myrow["role"] == "organizer"){
								// organizer
								$menu[PATH."events_list.php"] = "I miei eventi";
							}elseif($myrow["role"] == "customer"){
								// customer
								$menu[PATH."tickets.php"] = "Biglietti acquistati";
							}
						}
						$menu[PATH."calendar.php"] = "Calendario degli eventi";
						if(!LOGGED_IN || $myrow["role"] != "admin"){
							$menu[PATH."about_us.php"] = "Chi siamo";
						}
						if(LOGGED_IN){
							$menu[PATH."account.php"] = "Impostazioni account";
							$menu[PATH."logout.php"] = "Disconnettiti";
						}else{
							$menu[PATH."login.php"] = "Accedi";
							$menu[PATH."customer_registration.php"] = "Registrati come utente";
							$menu[PATH."organizer_registration.php"] = "Registrati come organizzatore";
						}
						foreach($menu as $link => $text){
							echo "<li>";
								echo "<a href=\"".$link."\">".$text."</a>";
							echo "</li>";
						}
					?>
				</ul>
			</nav>
		</div>
		<main>
