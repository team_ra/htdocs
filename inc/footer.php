		</main>
		<div id="alertcontainer">
			<section>
				<h1></h1>
				<p></p>
				<button name="ok"></button>
				<button name="cancel"></button>
			</section>
		</div>
		<footer id="rights">
			<img class="logo" alt="Logo sito: Outside" src="<?=PATH?>contents/logo_footer.png" />
			<p>
				Outside - Enjoy your life!
				<br />
				&copy; G. Amaducci, M. Desiderio, C. Teodorani
			</p>
			<a href="//instagram.com">
				<img class="social" src="<?=PATH?>contents/instagram_icon.png" alt="Instagram"/>
			</a>
			<a href="//facebook.com">
				<img class="social" src="<?=PATH?>contents/facebook_icon.png" alt="Facebook"/>
			</a>
		</footer>
	</body>
<html>
