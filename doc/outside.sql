-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2020 at 11:56 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `outside`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `security_code` varchar(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `user_id`, `security_code`) VALUES
(1, 1, '12345');

-- --------------------------------------------------------

--
-- Table structure for table `artists`
--

CREATE TABLE `artists` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `artists`
--

INSERT INTO `artists` (`id`, `name`) VALUES
(1, 'Sardine'),
(2, 'PD'),
(3, 'Carlo Cracco'),
(4, 'Mika'),
(5, 'Imagine Dragons'),
(6, 'Balletto Yacobson di San Pietroburgo'),
(7, 'Rihanna'),
(8, 'Lina Wertmüller'),
(9, 'Nazionale Italiana Tchoukball'),
(10, 'Food Valley'),
(11, 'Neri Marcorè'),
(12, 'Matteo Garrone');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Cucina'),
(2, 'Sport'),
(3, 'Musica'),
(4, 'Politica'),
(5, 'Ambiente'),
(6, 'Danza'),
(7, 'Teatro'),
(8, 'Libri'),
(9, 'Cinema'),
(10, 'Cibo');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `birthdate` date NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(2) NOT NULL,
  `zipcode` varchar(5) NOT NULL DEFAULT '''''',
  `registration_date` date NOT NULL,
  `confirmation_code` varchar(128) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(11) NOT NULL,
  `code` char(8) NOT NULL,
  `percentage` int(11) NOT NULL DEFAULT 0,
  `used_times` int(11) NOT NULL DEFAULT 0,
  `max_use_times` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL DEFAULT '',
  `image` varchar(200) NOT NULL DEFAULT '',
  `category_id` varchar(50) NOT NULL DEFAULT '',
  `organizer_user_id` int(11) NOT NULL DEFAULT 0,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `seats` int(4) NOT NULL DEFAULT 0,
  `address` varchar(150) NOT NULL,
  `program` varchar(200) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `approved` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `image`, `category_id`, `organizer_user_id`, `date`, `time`, `seats`, `address`, `program`, `price`, `approved`) VALUES
(1, 'Fridays For Future', 'fff.png', '5', 2, '2020-02-28', '09:30:00', 0, 'Piazza del Popolo, Ravenna', 'Portare striscioni di protesta', '0.00', '1'),
(2, 'Dibattito sulle elezioni regionali Emilia-Romagna', 'elezioni.jpg', '4', 2, '2020-01-09', '20:30:00', 200, 'Teatro Alighieri, Ravenna', 'Prenderanno parola i candidati dei vari partiti politici', '0.00', '1'),
(3, 'Corso di cucina', 'carlo_cracco.jpg', '1', 2, '2020-04-04', '16:30:00', 30, 'Ristorante Cracco, Milano', 'Lo Chef Carlo Cracco insegnerà un suo piatto famoso', '60.00', '1'),
(4, 'Concerto Mika', 'mika.jpg', '3', 2, '2020-02-07', '20:30:00', 5000, 'Palaflorio, Bari', 'Revelation Tour', '60.00', '1'),
(5, 'Concerto Imagine Dragons', 'imagine_dragons.jpg', '3', 2, '2020-09-06', '21:00:00', 10000, 'Rho Fiera, Milano', 'Milano Rocks - Alle 19:00 apriranno il concerto i Maneskin', '58.00', '1'),
(6, 'Schiaccianoci', 'schiaccianoci.jpg', '6', 2, '2020-12-05', '21:00:00', 300, 'Teatro ala Scala, Milano', 'Balletto in tre atti, con la partecipazione di Roberto Bolle', '120.00', '1'),
(7, 'Concerto Rihanna', 'rihanna.jpg', '3', 2, '2020-06-13', '21:00:00', 50000, 'Stadio San Siro, Milano', 'Anti-World Tour', '70.00', '1'),
(8, 'Presentazione Autobiografia', 'lina.jpg', '8', 2, '2020-09-21', '18:00:00', 100, 'Libreria Mondadori, Torino', 'Tutto a posto e niente in ordine', '0.00', '1'),
(9, 'Presentazione Autobiografia', 'lina.jpg', '8', 2, '2020-02-29', '18:00:00', 70, 'Libreria Mondadori, Napoli', 'Tutto a posto e niente in ordine', '0.00', '1'),
(10, 'Fridays For Future', 'fff.png', '5', 2, '2020-05-08', '09:30:00', 20000, 'Piazza Maggiore, Bologna', 'Portare striscioni', '0.00', '1'),
(11, 'Campionati Europei Tchoukball', 'tchk.jpg', '2', 2, '2020-08-02', '10:00:00', 200, 'Castellanza', 'Italia vs Svizzera M18 femminile', '0.00', '1'),
(12, 'Tour enogastronomico', 'parma.jpg', '10', 2, '2020-06-02', '11:30:00', 25, 'Parma', 'Degustazione di prodotti tipici nelle trattorie storiche', '35.00', '1'),
(13, 'Tango del calcio di rigore', 'tango.jpg', '7', 2, '2020-02-22', '21:00:00', 1300, 'Teatro celebrazioni, Bologna', 'Sul palco con Ugo Dighero', '30.00', '1'),
(14, 'Pinocchio', 'pinocchio.jpg', '9', 2, '2020-01-26', '20:30:00', 600, 'Cinema Fulgor, Rimini', 'In sala saranno presenti il regista Matteo Garrone e il protagonista Roberto Benigni', '15.00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `event_artists`
--

CREATE TABLE `event_artists` (
  `artist_id` int(11) NOT NULL DEFAULT 0,
  `event_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_artists`
--

INSERT INTO `event_artists` (`artist_id`, `event_id`) VALUES
(1, 1),
(1, 10),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(8, 9),
(9, 11),
(10, 12),
(11, 13),
(12, 14);

-- --------------------------------------------------------

--
-- Table structure for table `event_customers`
--

CREATE TABLE `event_customers` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL DEFAULT 0,
  `customer_user_id` int(11) NOT NULL DEFAULT 0,
  `sending_method` enum('physic','digital') NOT NULL DEFAULT 'physic',
  `name` varchar(128) NOT NULL DEFAULT 'NULL',
  `surname` varchar(128) NOT NULL DEFAULT 'NULL',
  `birthdate` varchar(128) NOT NULL DEFAULT 'NULL',
  `address` varchar(128) NOT NULL DEFAULT '',
  `city` varchar(128) NOT NULL DEFAULT '',
  `zipcode` varchar(5) NOT NULL DEFAULT '0',
  `country` varchar(128) NOT NULL DEFAULT '',
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `seats` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `organizers`
--

CREATE TABLE `organizers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `type` enum('company','private') NOT NULL DEFAULT 'company',
  `business_name` varchar(100) DEFAULT '',
  `id_number` char(16) DEFAULT '',
  `registration_date` date NOT NULL,
  `phone_number` varchar(15) NOT NULL DEFAULT '',
  `website` varchar(100) DEFAULT '',
  `approved` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `organizers`
--

INSERT INTO `organizers` (`id`, `user_id`, `type`, `business_name`, `id_number`, `registration_date`, `phone_number`, `website`, `approved`) VALUES
(8, 2, 'private', '', 'jaljflkajflajkdj', '2020-01-24', '33333333', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `event_id` int(11) NOT NULL DEFAULT 0,
  `number_tickets` int(4) NOT NULL,
  `type` enum('creditcard','paypal') NOT NULL,
  `discount_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) CHARACTER SET armscii8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `surname`) VALUES
(1, 'outsidevents@gmail.com', '72bda762515978455f719ccf4946c1bc', 'Outside', 'Events'),
(2, 'giada.amaducci.1998@gmail.com', '19590255af56d9764d052523fbcfdd8f', 'Giada', 'Amaducci');

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE `user_notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `text` varchar(500) NOT NULL,
  `datetime` datetime NOT NULL,
  `read` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_notifications`
--

INSERT INTO `user_notifications` (`id`, `user_id`, `text`, `datetime`, `read`) VALUES
(1, 1, 'C\'&egrave; un nuovo organizzatore in attesa di approvazione. <a href=\"http://localhost/user_manage.php?id=2\">Clicca qui per visualizzarlo</a>.', '2020-01-24 11:53:09', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `artists`
--
ALTER TABLE `artists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clients_ibfk_1` (`user_id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `events_ibfk_1` (`organizer_user_id`),
  ADD KEY `events_ibfk_2` (`category_id`);

--
-- Indexes for table `event_artists`
--
ALTER TABLE `event_artists`
  ADD PRIMARY KEY (`artist_id`,`event_id`),
  ADD KEY `artist_in_event_ibfk_2` (`event_id`);

--
-- Indexes for table `event_customers`
--
ALTER TABLE `event_customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tickets_ibfk_1` (`event_id`),
  ADD KEY `tickets_ibfk_2` (`customer_user_id`);

--
-- Indexes for table `organizers`
--
ALTER TABLE `organizers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organizers_ibfk_1` (`user_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_ibfk_1` (`user_id`),
  ADD KEY `payments_ibfk_2` (`event_id`),
  ADD KEY `payments_ibfk_3` (`discount_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_notifications_ibfk_1` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `artists`
--
ALTER TABLE `artists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `event_customers`
--
ALTER TABLE `event_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `organizers`
--
ALTER TABLE `organizers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `admins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`organizer_user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `event_artists`
--
ALTER TABLE `event_artists`
  ADD CONSTRAINT `event_artists_ibfk_1` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`),
  ADD CONSTRAINT `event_artists_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
