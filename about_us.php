<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Chi Siamo";

include(__DIR__."/inc/header.php");
?>

<section id="infosection">
    <h1>Chi siamo</h1>
    <br/>
    <h2>Contattaci</h2>
    <p>outsidevents@gmail.com</p>
    <br/>
    <h2>Creato da</h2>
    <p>&copy; Giada Amaducci, Marco Desiderio, Cecilia Teodorani</p>
    <br/>
    <h2>Account Social</h2>
    <a href="//instagram.com"><img class="social" src="<?=PATH?>contents/instagram_icon.png" alt="Instagram"/></a>
	<a href="//facebook.com"><img class="social" src="<?=PATH?>contents/facebook_icon.png" alt="Facebook"/></a>
</section>

<?php
    include(__DIR__."/inc/footer.php");
?>
