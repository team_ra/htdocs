<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Registrazione Organizzatore";

if(LOGGED_IN){
	location(PATH."account.php");
}

if(checkpost("do")){
	switch($_POST["do"]){
		case "register":
			if(!checkpost("name")){
				$output["message"] = "Inserisci il tuo nome";
			}elseif(!checkpost("surname")){
				$output["message"] = "Inserisci il tuo cognome";
			}elseif(!checkpost("type") || !in_array($_POST["type"], array("company", "private"))){
				$output["message"] = "Seleziona un tipo di account";
			}elseif($_POST["type"] == "company" && !checkpost("business_name")){
				$output["message"] = "Inserisci il nome dell'azienda";
			}elseif(!checkpost("id_number")){
				if($_POST["type"] == "company"){
					$output["message"] = "Inserisci la partita IVA dell'azienda";
				}else{
					$output["message"] = "Inserisci il tuo codice fiscale";
				}
			}elseif(!checkpost("email")){
				if($_POST["type"] == "company"){
					$output["message"] = "Inserisci l'indirizzo email dell'azienda";
				}else{
					$output["message"] = "Inserisci il tuo indirizzo email";
				}
			}elseif(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
				$output["message"] = "L'indirizzo email inserito non &egrave; valido";
			}elseif(num_rows(query("SELECT null FROM users WHERE email = '".escape($_POST["email"])."'")) != 0){
				$output["message"] = "Esiste gi&agrave; un utente con questo indirizzo email";
			}elseif(!checkpost("phone_number")){
				if($_POST["type"] == "company"){
					$output["message"] = "Inserisci il numero di telefono dell'azienda";
				}else{
					$output["message"] = "Inserisci il tuo numero di telefono";
				}
			}elseif(!is_numeric($_POST["phone_number"])){
				$output["message"] = "Il numero di telefono inserito non &egrave; valido";
			}elseif(!checkpost("privacy") || $_POST["privacy"] != 1){
				$output["message"] = "Devi accettare l'informativa sulla privacy";
			}elseif(!checkpost("g-recaptcha-response")){
				$output["message"] = "Non hai completato il controllo anti bot";
			}else{
				$req = curl_init();
				curl_setopt($req, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
				curl_setopt($req, CURLOPT_POST, true);
				curl_setopt($req, CURLOPT_POSTFIELDS, "secret=6LfX_8cUAAAAADZXpI5bbzyJAAn8PiX4UOE6Sw4Q&response=".$_POST["g-recaptcha-response"]."&remoteip=".MY_IP);
				curl_setopt($req, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36");
				curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($req, CURLOPT_CONNECTTIMEOUT ,3);
				curl_setopt($req, CURLOPT_TIMEOUT, 20);
				curl_setopt($req, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false);
				$response = curl_exec($req);
				curl_close($req);
				$response = json_decode($response, true);
				if($response["success"] !== true){
					$output["message"] = "Non hai completato il controllo anti bot";
				}else{
					query("INSERT INTO users (email, password, name, surname) VALUES ('".escape($_POST["email"])."', '', '".escape($_POST["name"])."', '".escape($_POST["surname"])."')");
					$user_id = insert_id();
					query("INSERT INTO organizers (user_id, type, business_name, id_number, registration_date, phone_number, website) VALUES ('".escape($user_id)."', '".escape($_POST["type"])."', '".escape($_POST["business_name"])."', '".escape($_POST["id_number"])."', CURDATE(), '".escape($_POST["phone_number"])."', '".escape($_POST["website"])."')");
					send_email($_POST["email"], "Account richiesto", entities($_POST["name"]).", grazie per aver richiesto di poter far parte di <span class='important'>Outside</span>!<br/><br/>La nostra politica prevede che tutti gli organizzatori siano approvati da un amministratore, perci&ograve; ora devi solo attendere. Entro 24 ore riceverai una nuova email con l'esito della tua richiesta.<br /><br />A presto!");
					$admins = query("SELECT u.email, u.id FROM users u, admins a WHERE a.user_id = u.id");
					while($admin = fetch($admins)){
						send_email($admin["email"], "Nuovo organizzatore", "&Egrave; stato richiesto un account dall'organizzatore <span class='important'>".entities($_POST["name"]." ".$_POST["surname"])."</span>".($_POST["type"]=="company"?" dell'azienda <span class='important'>".entities($_POST["business_name"])."</span>":"").". Accedi subito all'area riservata per visualizzare tutti i dettagli e decidere se approvare o rifiutare l'iscrizione.<br /><br /><a href=\"".PATH."login.php?email=".$admin["email"]."\" style=\"display: inline-block; border: 2px solid #56157E; padding: 5px 20px; color: #fff; background-color: #6a1b9a; border-radius: 5px; text-decoration: none; font-weight: bold;\">CLICCA QUI PER ACCEDERE</a>");
						query("INSERT INTO user_notifications (user_id, text, datetime, `read`) VALUES ('".escape($admin["id"])."', '".escape("C'&egrave; un nuovo organizzatore in attesa di approvazione. <a href=\"".PATH."user_manage.php?id=".$user_id."\">Clicca qui per visualizzarlo</a>.")."', NOW(), '0')");
					}
					$output["result"] = "success";
				}
			}
		break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<section id="organizerregistrationsection">
	<h1>Registrazione organizzatore</h1>
	<p>
		Se sei un organizzatore che vuole utilizzare Outside per pubblicizzare i propri eventi, compila il modulo per proporti.
		<a href="<?=PATH?>customer_registration.php">
			<button class="empty" aria-label="registrazione utente">Se invece sei un utente, clicca qui</button>
		</a>
	</p>
	<p>
		Per questioni di sicurezza, se sei un'azienda, ti chiediamo di registrarti con un indirizzo riconducibile all'azienda. Registrazioni con email non direttamente riconducibili all'azienda potrebbero essere rigettate.
	</p>
	<form>
		<input type="text" id="organizerregistrationsection_name" name="name" autofocus required />
        <label for="organizerregistrationsection_name">Nome</label>

		<input type="text" id="organizerregistrationsection_surname" name="surname" required />
        <label for="organizerregistrationsection_surname">Cognome</label>

		<select name="type" id="organizerregistrationsection_type">
			<option value="company">Azienda</option>
			<option value="private">Privato</option>
		</select>
		<label for="organizerregistrationsection_type">Tipo di organizzatore</label>

		<input type="text" id="organizerregistrationsection_business_name" name="business_name" />
		<label for="organizerregistrationsection_business_name">Nome azienda</label>

		<input type="text" id="organizerregistrationsection_id_number" name="id_number" required />
		<label for="organizerregistrationsection_id_number">Partita IVA</label>

		<input type="email" id="organizerregistrationsection_email" name="email" required />
		<label for="organizerregistrationsection_email">Email</label>

		<input type="tel" id="organizerregistrationsection_phone_number" name="phone_number" required />
		<label for="organizerregistrationsection_phone_number">Numero di telefono</label>

		<input type="url" id="organizerregistrationsection_website" name="website" />
		<label for="organizerregistrationsection_website">Sito internet</label>

		<div class="g-recaptcha" data-sitekey="6LfX_8cUAAAAAJV8_XGqYGZk17HtiZXXC-EvLIJ3"></div>

		<div class="privacybox">
			<div class="title main">INFORMATIVA PRIVACY AI SENSI DELL'ART. 13 REG. 679/2016 E NORME COLLEGATE</div>
			<br/>
			Ai sensi dell'art. 13 del Reg. 679/2016 (Regolamento per la protezione dei dati personali, detto anche GDPR") e norme collegate, con riferimento ai suoi dati personali e/o a quelli del minore (d'ora in avanti detti l'Interessato - cioè la persona fisica a cui si riferiscono i dati personali raccolti e trattati) su cui esercita la responsabilità genitoriale di cui i responsabili del trattamento dei dati entreranno in possesso, desideriamo informarla di quanto segue:<br/>
			<br/>
			<div class="title">1. Finalità di trattamento dei dati </div>
			I dati personali da lei forniti saranno trattati per le seguenti finalità:<br/>
			a) riconoscimento dell'utente all'interno della piattaforma attraverso un account;<br/>
			b) inserimento dei dati anagrafici richiesti nei database informatici;<br/>
			c) tenuta della documentazione e svolgimento della gestione contabile, incassi e pagamenti;<br/>
			d) adempimento di tutti gli obblighi di legge connessi allo svolgimento e all'organizzazione dell'attività della piattaforma;<br/>
			e) promozione dell'attività della piattaforma;<br/>
			f) elaborazione di studi, ricerche statistiche e di mercato, profili utenti a scopi commerciali e di marketing;<br/>
			g) invio di materiale pubblicitario ed informativo;<br/>
			h) cessione o concessione a terzi dei dati personali anche a scopo di lucro o per finalità commerciali.<br/>
			<br/>
			<div class="title">2. Modalità di trattamento dei dati </div>
			2.1 Il trattamento dei dati personali avviene mediante elaborazioni manuali o strumenti automatizzati per tutto il tempo in cui la piattaforma sarà attiva e/o l'account sarà attivo. Specifiche misure di sicurezza sono osservate per prevenire la perdita di dati, usi illeciti o non corretti ed accessi non autorizzati.<br/>
			2.2 Il trattamento di dati personali di terzi, forniti dall'Interessato, sarà possibile per le finalità suindicate; l'Interessato avrà cura di fornire al terzo l'informativa relativa al presente trattamento, raccogliendo la sua autorizzazione al trattamento se dovuta.<br/>
			<br/>
			<div class="title">3. Natura dei dati </div>
			I dati trattati dall'associazione saranno tutti quelli richiesti dalla stessa per iscritto (a titolo esemplificativo: dati anagrafici, contatti, indirizzi telematici ecc.), o comunque conferiti nel corso dell'utilizzo della piattaforma.<br/>
			<br/>
			<div class="title">4. Diffusione dei dati personali </div>
			I dati personali forniti potranno essere oggetto di diffusione esclusivamente agli organizzatori di eventi che, per tutela della compravendita, devono essere a conoscenza di chi ne acquista o prenota gratuitamente i biglietti.<br/>
			<br/>
			<div class="title">5. Comunicazione dei dati </div>
			Al trattamento dei dati personali saranno applicate tutte le misure di sicurezza, tecniche ed organizzative, ritenute idonee dal Titolare ai sensi dell'art. 32 GDPR. I dati non saranno oggetto di trasferimento al di fuori del territorio dell'Unione Europea, se non previa informativa all'Interessato e con illustrazione dell'adeguatezza oppure delle garanzie appropriate o delle deroghe alle stesse applicabili, ex artt. 42-50 GDPR.<br/>
			I suoi dati personali non verranno comunicati, ceduti o licenziati a terzi per finalità commerciale.<br/>
			<br/>
			<div class="title">6. Dati di terzi </div>
			La responsabilità relativa ai dati personali di soggetti terzi, che venissero dalla piattaforma trattati in ogni modo a seguito di comunicazione e/o diffusione da parte sua, sarà imputabile esclusivamente a Lei, che garantisce di avere il diritto di comunicarli e diffonderli e libera il titolare del trattamento nei confronti dei terzi interessati.<br/>
			I dati di terzi di cui Lei viene in possesso, sono da considerarsi utilizzabili esclusivamente per l'utilizzo nella circoscritta situazione di gestione dell'evento e di analisi dei partecipanti ad esso.<br/>
			<br/>
			<div class="title">7. Diritti dell'interessato </div>
			7.1 I diritti spettanti all'Interessato sono i seguenti ex artt. 7, 13-22 GDPR, per quanto applicabili al singolo caso concreto:
			a) ha il diritto di chiedere al Titolare del trattamento l'accesso ai suoi dati personali, chiedendo conferma o meno della loro esistenza nonché la rettifica o la cancellazione degli stessi o la limitazione (blocco temporaneo) del trattamento che lo riguarda;<br/>
			b) se ha fornito il consenso per una o più specifiche finalità, ha il diritto di revocare tale consenso in qualsiasi momento;<br/>
			c) ha il diritto di proporre reclamo alla seguente Autorità di Controllo: Garante per la protezione dei dati personali (<a href="http://www.garanteprivacy.it" target="_blank">http://www.garanteprivacy.it</a>); ha comunque facoltà di proporre alternativamente reclamo all'autorità competente dello Stato membro ove risiede abitualmente, lavora o del luogo ove si è verificata la presunta violazione;<br/>
			d) ha il diritto alla portabilità dei suoi dati personali (per quelli con base legale di esecuzione contrattuale o consensuale) mediante richiesta al Titolare, a mezzo di comunicazione di un file in formato .csv, .xml o analogo;<br/>
			e) ha il diritto di OPPORSI in qualsiasi momento al loro trattamento per motivi connessi alla sua situazione particolare in caso di trattamento necessario per l'esecuzione di un compito di interesse pubblico o connesso all'esercizio di pubblici poteri, oppure in caso di perseguimento di legittimo interesse del Titolare<br/>
			7.2. Il trattamento avviene mediante processi automatizzati che non determinano la profilazione degli Interessati.<br/>
			7.3. Per esercitare i suddetti diritti l'Interessato può contattare i titolari al recapito contrattuale.<br/>
			<br/>
			<div class="title">8. Titolare del Trattamento </div>
			Titolari del trattamento per l'esercizio dei suoi diritti sono i seguenti:<br/>
			Giada Amaducci, Marco Desiderio, Cecilia Teodorani<br/><a href="mailto:outsidevents@gmail.com">outsidevents@gmail.com</a><br/>
			<br/>
			Per ogni chiarimento terminologico, si veda la guida del Garante: <a href="https://www.garanteprivacy.it/regolamentoue" target="_blank">https://www.garanteprivacy.it/regolamentoue</a>.
		</div>

		<label for="customerregistrationsection_privacy">Accetto l'informativa sulla privacy</label>
		<input type="checkbox" id="customerregistrationsection_privacy" name="privacy" />

		<input type="submit" name="register" value="Richiedi un account" />
	</form>
</section>
<script>
	$("section#organizerregistrationsection form").on("submit", function(e){
		e.preventDefault();
		formPost("organizerregistrationsection", function(data){
			if(checkData(data)){
				openAlert({
					title: "Fatto",
					text: "Abbiamo ricevuto la tua richiesta di registrazione. Un amministratore la vaglier&agrave; e riceverai un'email entro 24 ore con il risultato della tua richiesta.",
					okbutton: {
						text: "Ok",
						onclick: function(){
							loc("<?=PATH?>login.php");
						},
						close: false
					}
				});
			}else{
				grecaptcha.reset();
			}
		});
	});
</script>
<?php
include(__DIR__."/inc/footer.php");
?>
