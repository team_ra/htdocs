<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Calendario";

if(checkpost("do")){
	switch($_POST["do"]){
		case "getcalendar":
			$output["days"] = array();
			$events = query("SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_format FROM events WHERE approved = '1' AND YEAR(date) = '".escape($_POST["year"])."' AND MONTH(date) = '".escape($_POST["month"])."'");
			$eventslist = array();
			while($event = fetch($events)){
				if(!isset($eventslist[$event["date_format"]])){
					$eventslist[$event["date_format"]] = array();
				}
				$eventslist[$event["date_format"]][] = $event;
			}
			for($i=strtotime("01-".$_POST["month"]."-".$_POST["year"]);$i<strtotime("01-".$_POST["month"]."-".$_POST["year"]." + 1 month");$i=strtotime(date("d-m-Y", $i)." + 1 day")){
				$output["days"][] = array("day" => date("d", $i), "weekday" => date("N", $i), "events" => (isset($eventslist[date("d-m-Y", $i)])?$eventslist[date("d-m-Y", $i)]:array()));
			}
			$output["thismonth"] = month($_POST["month"])." ".$_POST["year"];
			$output["prev"] = array("month" => date("m", strtotime($_POST["year"]."/".$_POST["month"]."/01 - 1 month")), "year" => date("Y", strtotime($_POST["year"]."/".$_POST["month"]."/01 - 1 month")));
			$output["next"] = array("month" => date("m", strtotime($_POST["year"]."/".$_POST["month"]."/01 + 1 month")), "year" => date("Y", strtotime($_POST["year"]."/".$_POST["month"]."/01 + 1 month")));
			$output["result"] = "success";
			break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<section>
	<h1>Calendario Eventi</h1>
	<div class="calendar"></div>
	<div class="mobilecalendar"></div>
</section>


<script>
function loadCalendar(month, year){
	$("div.calendar").html("<div class=\"loading\"></div>");
	post({
		data: {
			"do": "getcalendar",
			"month": month,
			"year": year
		},
		callback: function(data){
			if(data["result"] == "success"){
				// desktop
				let htmloutput = "<table><tr class=\"header\"><td class=\"prev disabled\"><button aria-label=\"mese precedente\">&laquo;</button></td><td colspan=\"5\">"+data["thismonth"]+"</td><td class=\"next disabled\"><button aria-label=\"mese successivo\">&raquo;</button></td></tr><tr class=\"header weekdays\"><td>LUN</td><td>MAR</td><td>MER</td><td>GIO</td><td>VEN</td><td>SAB</td><td>DOM</td></tr><tr>";
				if(data["days"][0]["weekday"] > 1){
					htmloutput += "<td colspan=\""+(data["days"][0]["weekday"]-1)+"\"></td>";
				}
				for(let i=0;i<data["days"].length;i++){
					if(data["days"][i]["weekday"] == 1){
						htmloutput += "</tr><tr>";
					}
					htmloutput += "<td><div class=\"day\">"+data["days"][i]["day"];
						for(let j=0;j<data["days"][i]["events"].length;j++){
							htmloutput += "<a href=\"<?=PATH?>event.php?id="+data["days"][i]["events"][j]["id"]+"\"><button>"+data["days"][i]["events"][j]["name"]+"</button></a>";
						}
					htmloutput += "</div></td>";
				}
				if(data["days"][data["days"].length-1]["weekday"] < 7){
					htmloutput += "<td colspan=\""+(7-data["days"][data["days"].length-1]["weekday"])+"\"></td>";
				}
				htmloutput += "</tr></table>";
				$("div.calendar").html(htmloutput);
				// mobile
				htmloutput = "<table><tr class=\"header\"><td class=\"prev disabled\"><button>&laquo;</button></td><td colspan=\"5\">"+data["thismonth"]+"</td><td class=\"next disabled\"><button>&raquo;</button></td></tr></table>";
				let printed_something = false;
				for(let i=0;i<data["days"].length;i++){
					if(data["days"][i]["events"].length > 0){
						printed_something = true;
						htmloutput += "<div class=\"day\">"+data["days"][i]["day"];
						for(let j=0;j<data["days"][i]["events"].length;j++){
							htmloutput += "<a href=\"<?=PATH?>event.php?id="+data["days"][i]["events"][j]["id"]+"\"><button>"+data["days"][i]["events"][j]["name"]+"</button></a>";
						}
						htmloutput += "</div>";
					}
				}
				if(!printed_something){
					htmloutput += "<div class='main italic'>Non ci sono eventi programmati per questo mese</div>";
				}
				$("div.mobilecalendar").html(htmloutput);
				if(data["prev"] !== null){
					$("div.calendar tr.header td.prev").removeClass("disabled");
					$("div.calendar tr.header td.prev").on("click", function(){
						loadCalendar(data["prev"]["month"], data["prev"]["year"]);
					});
					$("div.mobilecalendar tr.header td.prev").removeClass("disabled");
					$("div.mobilecalendar tr.header td.prev").on("click", function(){
						loadCalendar(data["prev"]["month"], data["prev"]["year"]);
					});
				}
				if(data["next"] !== null){
					$("div.calendar tr.header td.next").removeClass("disabled");
					$("div.calendar tr.header td.next").on("click", function(){
						loadCalendar(data["next"]["month"], data["next"]["year"]);
					});
					$("div.mobilecalendar tr.header td.next").removeClass("disabled");
					$("div.mobilecalendar tr.header td.next").on("click", function(){
						loadCalendar(data["next"]["month"], data["next"]["year"]);
					});
				}
			}
		}
	});
}
loadCalendar("<?php echo date("m"); ?>", "<?php echo date("Y"); ?>");
</script>
<?php
    include(__DIR__."/inc/footer.php");
?>
