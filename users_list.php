<?php
require_once(__DIR__."/inc/core.php");

if(!LOGGED_IN || $myrow["role"] != "admin"){
	location(PATH);
}

if(checkpost("do")){
    switch($_POST["do"]){
		case "search":
			$wheres = array("1=1");
			if(checkpost("filterrole") && in_array($_POST["filterrole"], array("organizers", "customers", "admins"))){
				$wheres[] = "EXISTS (SELECT null FROM ".$_POST["filterrole"]." WHERE user_id = u.id)";
			}
			if(checkpost("searchvalue")){
				$wheres[] = "(CONCAT(name,' ',surname) LIKE '%".escape($_POST["searchvalue"])."%' OR CONCAT(surname,' ',name) LIKE '%".escape($_POST["searchvalue"])."%' OR email LIKE '%".escape($_POST["searchvalue"])."%')";
			}
			$users = query("SELECT
								u.*,
								(SELECT COUNT(*) FROM customers WHERE user_id = u.id) as is_customer,
								(SELECT COUNT(*) FROM organizers WHERE user_id = u.id) as is_organizer,
								(SELECT COUNT(*) FROM admins WHERE user_id = u.id) as is_admin
							FROM
								users u
							WHERE
								".join(" AND ", $wheres)."
							ORDER BY
								name, surname");
			$output["users"] = array();
			while($user = fetch($users)){
				$output["users"][] = array_map("entities", $user);
			}
			$output["result"] = "success";
			break;
    }
    output();
}

$pagetitle = "Gestisci utenti";
include(__DIR__."/inc/header.php");
?>
<a class="fullbuttoncontainer" href="<?=PATH?>admin_registration.php">
	<button>Aggiungi un nuovo admin</button>
</a>
<section id="userssection">
	<h1>Organizzatori da approvare</h1>
	<?php
		$organizers = query("SELECT u.id, u.name, u.surname, o.type, o.business_name, u.email FROM users u, organizers o WHERE o.user_id = u.id AND o.approved = '0'");
		while($organizer = fetch($organizers)){
		?>
			<a href="<?=PATH?>user_manage.php?id=<?=$organizer["id"]?>">
				<div class="infobox">
					<span class="important"><?=entities($organizer["name"]." ".$organizer["surname"])?></span>
					<footer>
						<?php
							if($organizer["type"] == "private"){
								echo "Privato";
							}else{
								echo "Azienda: ".entities($organizer["business_name"]);
							}
						?>
						<br />
						<?=entities($organizer["email"])?>
					</footer>
				</div>
			</a>
		<?php
		}
	?>
</section>
<section id="userssection" class="margintop">
	<h1>Elenco utenti</h1>
    <form>
		<fieldset>
			<legend>Filtri</legend>
			<div class="filter">
				<label for="userssection_filter_organizers">Organizzatori</label>
				<input type="radio" name="filterrole" id="userssection_filter_organizers" value="organizers" checked />
			</div>
			<div class="filter">
				<label for="userssection_filter_customers">Utenti</label>
				<input type="radio" name="filterrole" id="userssection_filter_customers" value="customers" />
			</div>
			<div class="filter">
				<label for="userssection_filter_admins">Admin</label>
				<input type="radio" name="filterrole" id="userssection_filter_admins" value="admins" />
			</div>
			<div class="filter">
				<label for="userssection_filter_all">Tutti</label>
				<input type="radio" name="filterrole" id="userssection_filter_all" value="all" />
			</div>
		</fieldset>
    	<div class="searchbox">
            <input type="text" name="searchvalue" placeholder="Cerca un utente..." />
            <input type="submit" name="search" value="" />
		</div>
	</form>
	<div id="userssection_users"></div>
</section>
<script>
    $("section#userssection form").on("submit", function(e){
		e.preventDefault();
		$("#userssection_users").html("<div class=\"loading\"></div>");
        formPost("userssection", function(data){
			if(checkData(data)){
				let htmloutput = "";
				if(data["users"].length == 0){
					htmloutput += "<div class=\"main italic\">Nessun utente trovato</div>";
				}else{
					for(let i=0;i<data["users"].length;i++){
						htmloutput += "<a href=\"<?=PATH?>user_manage.php?id="+data["users"][i]["id"]+"\">";
							htmloutput += "<div class=\"infobox\">";
								htmloutput += "<span class='important'>"+data["users"][i]["name"]+" "+data["users"][i]["surname"]+"</span>";
								htmloutput += "<footer>";
									if(data["users"][i]["is_organizer"] == "1"){
										htmloutput += "Organizzatore";
									}
									if(data["users"][i]["is_customer"] == "1"){
										htmloutput += "Utente";
									}
									if(data["users"][i]["is_admin"] == "1"){
										htmloutput += "Amministratore";
									}
								htmloutput += "<br />"+data["users"][i]["email"]+"</footer>";
							htmloutput += "</div>";
						htmloutput += "</a>";
					}
				}
				$("#userssection_users").html(htmloutput);
			}
		}, false);
    });
	$("section#userssection form").submit();
</script>
<?php
    include(__DIR__."/inc/footer.php");
?>
