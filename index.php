<?php
require_once(__DIR__."/inc/core.php");

if(LOGGED_IN && $myrow["role"]=="admin"){
    location(PATH."account.php");
}

include(__DIR__."/inc/header.php");
?>
<section class="neweventssection">
	<h1 class="marginbottom">I prossimi eventi</h1>
	<div class="slider slideshow">
		<ul class="swiper-wrapper">
			<?php
				$nextevents = query("SELECT id, image, name, address, DATE_FORMAT(date, '%d-%m-%Y') as date FROM events WHERE approved = '1' ORDER BY date DESC LIMIT 5");
				while($event = fetch($nextevents)){
			?>
				<a class="swiper-slide" href="<?=PATH?>event.php?id=<?=$event["id"]?>">
					<li style="background-image: url(<?=PATH?>contents/uploads/<?=$event["image"]?>);">
						<div class="shadow"></div>
						<div class="info">
							<div class="name"><?=entities($event["name"])?></div>
							<?=entities($event["address"]." - ".$event["date"])?>
						</div>
					</li>
				</a>
			<?php
				}
			?>
		</ul>
		<div class="pagination"></div>
		<button class="custom left"></button>
		<button class="custom right"></button>
	</div>
    <form method="get" action="<?=PATH?>events.php">
    	<div class="searchbox">
			<input type="text" id="searchvalue" name="searchvalue" placeholder="Cerca un evento intorno a te..." />
			<label class="hidden" for="searchvalue">Cerca</label>
			<input type="submit" name="search" value="" />
		</div>
	</form>
	<h1 class="margintop">Eventi per categoria</h1>
	<?php
		$categories = query("SELECT * FROM categories ORDER BY name");
		while($category = fetch($categories)){
	?>
			<h2 class="margintop marginbottom"><?=entities(ucfirst($category["name"]))?></h2>
			<div class="slider" data-slidesperview="2.5">
				<ul class="swiper-wrapper">
					<?php
						$categoryevents = query("SELECT id, image, name, address, DATE_FORMAT(date, '%d-%m-%Y') as date FROM events WHERE approved = '1' AND category_id = '".escape($category["id"])."' ORDER BY date DESC LIMIT 10");
						while($event = fetch($categoryevents)){
					?>
						<a class="swiper-slide" href="<?=PATH?>event.php?id=<?=$event["id"]?>">
							<li style="background-image: url(<?=PATH?>contents/uploads/<?=$event["image"]?>);">
								<div class="shadow"></div>
								<div class="info">
									<div class="name"><?=entities($event["name"])?></div>
								</div>
							</li>
						</a>
					<?php
						}
					?>
				</ul>
				<div class="pagination"></div>
				<button class="custom left"></button>
				<button class="custom right"></button>
			</div>
	<?php
		}
	?>
</section>
<?php
include(__DIR__."/inc/footer.php");
?>
