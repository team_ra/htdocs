<?php
require_once(__DIR__."/inc/core.php");

if(isset($_SESSION["cartdetails"]["discount"])){
	$discount = query("SELECT percentage FROM discounts WHERE code = '".escape($_SESSION["cartdetails"]["discount"])."' AND used_times < max_use_times");
	if(num_rows($discount) == 1){
		$discount = fetch($discount);
		$discount = (1 - ($discount["percentage"] / 100));
	}
}

if(isset($_POST["ok"])){
	while($event = fetch($cartevents)){
		query("INSERT INTO event_customers (event_id, customer_user_id, sending_method, name, surname, birthdate, address, city, zipcode, country, price, seats) VALUES ('".escape($event["id"])."', '".escape($myrow["id"])."', '".escape($_SESSION["cartdetails"]["sendingmethod"])."', '".escape($_SESSION["cartdetails"]["name"])."', '".escape($_SESSION["cartdetails"]["surname"])."', '".escape($_SESSION["cartdetails"]["birthdate"])."', '".escape($_SESSION["cartdetails"]["address"])."', '".escape($_SESSION["cartdetails"]["city"])."', '".escape($_SESSION["cartdetails"]["zipcode"])."', '".escape($_SESSION["cartdetails"]["country"])."', '".escape($event["price"]*$discount)."', '".escape($_SESSION["cart"][$event["id"]])."')");

		$ticketscount = 0;
		$tickets = query("SELECT seats FROM event_customers WHERE event_id = '".escape($event["id"])."'");
		while($ticket = fetch($tickets)){
			$ticketscount += $ticket["seats"];
		}
		if($ticketscount == $event["seats"]){
			// sold out
			$organizer = fetch(query("SELECT name, email FROM users WHERE id = (SELECT user_id FROM organizers WHERE id = '".escape($event["organizer_user_id"])."')"));
			send_email($organizer["email"], entities($event["name"])." &egrave; SOLD OUT!", entities($organizer["name"]).", il tuo evento <b>".entities($event["name"])."</b> &egrave; ora <b>SOLD OUT</b>! Significa che hai venduto TUUUUUTTI i biglietti che potevi, grazie ad Outside!<br />Ora non ti resta che preparare l'evento.<br /><br />Per qualsiasi dubbio restiamo a disposizione!");
			query("INSERT INTO user_notifications (user_id, text, datetime, `read`) VALUES ('".escape($event["organizer_user_id"])."', '".escape("Il tuo evento <b>".entities($event["name"])."</b> &egrave; SOLD OUT. <a href=\"".PATH."event.php?id=".$event["id"]."\">Clicca qui per visualizzarlo</a>.")."', NOW(), '0')");
		}
	}
	if(isset($_SESSION["cartdetails"]["discount"])){
		query("UPDATE discounts SET used_times = used_times+1 WHERE code = '".escape($_SESSION["cartdetails"]["discount"])."'");
	}
	unset($_SESSION["cart"]);
	unset($_SESSION["cartdetails"]);
?>
<script>
	opener.confirmPayment();
	window.close();
</script>
<?php
	exit;
}


$totalprice = 0;
$eventslist = array();
$events = query("SELECT e.id, e.name, DATE_FORMAT(e.date, '%d-%m-%Y') as date, e.price, IF(o.type='private',(SELECT CONCAT(name,' ',surname) FROM users WHERE id = o.user_id),o.business_name) as organizer_name FROM events e, organizers o WHERE e.organizer_user_id = o.user_id AND e.approved = '1' AND e.id IN ('".join("','", array_keys($_SESSION["cart"]))."')");
while($event = fetch($events)){
	$eventslist[$event["id"]] = $event;
}
foreach($_SESSION["cart"] as $eventid => $tickets){
	$totalprice += $tickets*$eventslist[$eventid]["price"];
}
$totalprice = $totalprice * $discount;
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8" />
		<title>Completa il pagamento</title>
		<link rel="stylesheet" href="<?=PATH?>contents/pay_style.css?<?=time()?>"/>
	</head>
	<body>
		<h1>Completa il pagamento</h1>
		<h2><?=number_format($totalprice, 2, ",", "")?>&euro;</h2>
		<span class="important">Paga con Carta di Credito</span>
		<br/>
		<br/>
		<table>
			<tr>
				<td>
					Nome intestatario:
				</td>
				<td colspan="2">
					<label>
						Cognome intestatario:
					</label>
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" />
				</td>
				<td colspan="2">
					<input type="text" />
				</td>
			</tr>
			<tr>
				<td colspan="3"><br/><br/></td>
			</tr>
			<tr>
				<td>
					<label>
						Codice carta:
						<input type="text" />
					</label>
				</td>
				<td>
					<label>
						CVV:
						<input type="text" size="5" />
					</label>
				</td>
				<td>
					<label>
						Scadenza (MM/AA):
						<input type="text" size="7" />
					</label>
				</td>
			</tr>
		</table>
		<br/>
		<br/>
		<form method="post">
			<input type="submit" name="ok" value="Conferma pagamento">
		</form>
		<br/>
		<a href="#">Clicca qui se vuoi pagare con PayPal</a>
	</body>
</html>
