<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Biglietti Acquistati";

if(!LOGGED_IN || $myrow["role"] != "customer"){
	location(PATH."login.php");
}

include(__DIR__."/inc/header.php");
?>
<section id="ticketsection">
	<h1>Biglietti Acquistati</h1>
	<?php
		$tickets = query("SELECT e.name,
								DATE_FORMAT(e.date, '%d-%m-%Y') as date,
								TIME_FORMAT(e.time, '%H:%i') as time,
								ec.seats,
                                ec.price,
                                e.id
						 FROM events e, event_customers ec
						 WHERE e.id = ec.event_id AND
                               ec.customer_user_id='".escape($myrow["id"])."'");
        if(num_rows($tickets) == 0){?>
            <p>Non hai ancora acquistato dei biglietti!</p>
        <?php
        }else{
            while($ticket = fetch($tickets)){
        ?>
				<a href="<?=PATH?>event.php?id=<?=$ticket["id"]?>">
	                <div class="infobox">
	                    <span class="important"><?=entities($ticket["name"])?></span>
						<br/>
	                    <footer>
	                        <?=pretty_date(entities($ticket["date"]))?>, ore <?=entities($ticket["time"])?><br/>
	                        Numero biglietti: <?=entities($ticket["seats"])?><br/>
	                        <?php if($ticket["price"] == 0){ ?>
								Gratuito
	                        <?php }else{ ?>
								Pagato <?=number_format($ticket["price"]*$ticket["seats"], 2, ",", "")?>&euro;
	                        <?php } ?>
	                    </footer>
	                </div>
				</a>
        <?php
            }
        }
	    ?>
</section>

<?php
    include(__DIR__."/inc/footer.php");
?>
